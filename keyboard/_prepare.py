import os
import pathlib
import shutil
import subprocess
import sys


PATH = pathlib.Path(__file__).parent


path = pathlib.Path(os.environ["ZERO_PATH"], "keyboard", "keyboard-master")
windows_path = pathlib.Path(path, "windows")

_ = [sys.executable, str(pathlib.Path(windows_path, "_prepare_mpw.py"))]
assert subprocess.Popen(_).wait() == 0

_ = [sys.executable, str(pathlib.Path(windows_path, "mpw", "build.py"))]
assert subprocess.Popen(_).wait() == 0

print("> creating keyboard.mpw files")
_ = ";".join(map(str, [path, pathlib.Path(path, "keyboard", "windows")]))
env = os.environ | dict(PYTHONPATH=_)
for name, file_name in [("personal", "password"), ("work", "work password")]:
    print(repr(name), end=" ")
    sys.stdout.flush()

    _ = [sys.executable, "-m", "pipenv", "run", "python", "-m", "keyboard.mpw"]
    process = subprocess.Popen(_, cwd=windows_path, env=env, stdout=subprocess.PIPE)
    with open(pathlib.Path(PATH, file_name), "wb") as file:
        shutil.copyfileobj(process.stdout, file)

    assert process.wait() == 0
