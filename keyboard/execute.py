import pathlib
import subprocess
import sys


PATH = pathlib.Path(__file__).parent


_ = [
    sys.executable,
    str(pathlib.Path(PATH.parent, "zero-master", "keyboard", "execute.py")),
    "--mpw",
    "--",
    str(pathlib.Path(PATH, "main.py")),
    "--windows",
    "--german",
]
assert subprocess.Popen(_).wait() == 0
