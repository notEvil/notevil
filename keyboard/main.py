import keyboard
import keyboard.mpw as k_mpw
import evdev.ecodes as e_ecodes
import argparse
import pathlib


PATH = pathlib.Path(__file__).parent


parser = argparse.ArgumentParser()

parser.add_argument("--device", default=None, help="Linux: path to keyboard device.")
parser.add_argument("--windows", default=False, action="store_true")
parser.add_argument(
    "--german",
    default=False,
    action="store_true",
    help="Windows: base layout is German, otherwise US.",
)
parser.add_argument("--ansi", default=False, action="store_true")

arguments = parser.parse_args()
assert (arguments.device is not None) ^ arguments.windows


if arguments.windows:
    import keyboard.windows as k_windows


key_codes = {
    # e_ecodes.KEY_ESC: None,
    e_ecodes.KEY_BACKSPACE: None,
    # e_ecodes.KEY_TAB: None,
    e_ecodes.KEY_CAPSLOCK: e_ecodes.KEY_BACKSPACE,
    # e_ecodes.KEY_LEFTSHIFT: e_ecodes.KEY_ESC,
    # e_ecodes.KEY_RIGHTSHIFT: e_ecodes.KEY_TAB,
    # e_ecodes.KEY_SPACE: e_ecodes.KEY_LEFTSHIFT,
}
if not arguments.ansi:
    _ = e_ecodes.KEY_BACKSLASH
    _ = {_: e_ecodes.KEY_ENTER, e_ecodes.KEY_102ND: e_ecodes.KEY_BACKSLASH}
    key_codes.update(_)


_none = frozenset()
_shift = frozenset([keyboard.SHIFT])

if arguments.windows and arguments.german:
    _ralt = frozenset([keyboard.CTRL, keyboard.LEFT_ALT])
    _shift_ralt = frozenset([keyboard.SHIFT, keyboard.CTRL, keyboard.LEFT_ALT])

else:
    _ralt = frozenset([keyboard.RIGHT_ALT])
    _shift_ralt = frozenset([keyboard.SHIFT, keyboard.RIGHT_ALT])


characters = {
    (e_ecodes.KEY_0, _none): "0",
    (e_ecodes.KEY_0, _shift): ")",
    (e_ecodes.KEY_1, _none): "1",
    (e_ecodes.KEY_1, _shift): "!",
    (e_ecodes.KEY_102ND, _none): "\\",
    (e_ecodes.KEY_102ND, _shift): "|",
    (e_ecodes.KEY_2, _none): "2",
    (e_ecodes.KEY_2, _shift): "@",
    (e_ecodes.KEY_3, _none): "3",
    (e_ecodes.KEY_3, _shift): "#",
    (e_ecodes.KEY_4, _none): "4",
    (e_ecodes.KEY_4, _shift): "$",
    (e_ecodes.KEY_5, _none): "5",
    (e_ecodes.KEY_5, _shift): "%",
    (e_ecodes.KEY_6, _none): "6",
    (e_ecodes.KEY_6, _shift): "^",
    (e_ecodes.KEY_7, _none): "7",
    (e_ecodes.KEY_7, _shift): "&",
    (e_ecodes.KEY_8, _none): "8",
    (e_ecodes.KEY_8, _shift): "*",
    (e_ecodes.KEY_9, _none): "9",
    (e_ecodes.KEY_9, _shift): "(",
    (e_ecodes.KEY_A, _none): "a",
    (e_ecodes.KEY_A, _shift): "A",
    (e_ecodes.KEY_APOSTROPHE, _none): "'",
    (e_ecodes.KEY_APOSTROPHE, _shift): '"',
    (e_ecodes.KEY_B, _none): "b",
    (e_ecodes.KEY_B, _shift): "B",
    (e_ecodes.KEY_BACKSLASH, _none): "\\",
    (e_ecodes.KEY_BACKSLASH, _shift): "|",
    # (e_ecodes.KEY_BACKSPACE, ("CONTROL",)): "\x7f",
    (e_ecodes.KEY_C, _none): "c",
    (e_ecodes.KEY_C, _shift): "C",
    (e_ecodes.KEY_COMMA, _none): ",",
    (e_ecodes.KEY_COMMA, _shift): "<",
    (e_ecodes.KEY_D, _none): "s",
    (e_ecodes.KEY_D, _shift): "S",
    (e_ecodes.KEY_DOT, _none): ".",
    (e_ecodes.KEY_DOT, _shift): ">",
    (e_ecodes.KEY_E, _none): "f",
    (e_ecodes.KEY_E, _shift): "F",
    (e_ecodes.KEY_EQUAL, _none): "=",
    (e_ecodes.KEY_EQUAL, _shift): "+",
    (e_ecodes.KEY_F, _none): "t",
    (e_ecodes.KEY_F, _shift): "T",
    (e_ecodes.KEY_G, _none): "d",
    (e_ecodes.KEY_G, _shift): "D",
    (e_ecodes.KEY_GRAVE, _none): "`",
    (e_ecodes.KEY_GRAVE, _shift): "~",
    (e_ecodes.KEY_H, _none): "h",
    (e_ecodes.KEY_H, _shift): "H",
    (e_ecodes.KEY_I, _none): "u",
    (e_ecodes.KEY_I, _shift): "U",
    (e_ecodes.KEY_J, _none): "n",
    (e_ecodes.KEY_J, _shift): "N",
    (e_ecodes.KEY_K, _none): "e",
    (e_ecodes.KEY_K, _shift): "E",
    # (e_ecodes.KEY_KP0, ("NUMLOCK",)): "0",
    # (e_ecodes.KEY_KP1, ("NUMLOCK",)): "1",
    # (e_ecodes.KEY_KP2, ("NUMLOCK",)): "2",
    # (e_ecodes.KEY_KP3, ("NUMLOCK",)): "3",
    # (e_ecodes.KEY_KP4, ("NUMLOCK",)): "4",
    # (e_ecodes.KEY_KP5, ("NUMLOCK",)): "5",
    # (e_ecodes.KEY_KP6, ("NUMLOCK",)): "6",
    # (e_ecodes.KEY_KP7, ("NUMLOCK",)): "7",
    # (e_ecodes.KEY_KP8, ("NUMLOCK",)): "8",
    # (e_ecodes.KEY_KP9, ("NUMLOCK",)): "9",
    (e_ecodes.KEY_KPASTERISK, _none): "*",
    # (e_ecodes.KEY_KPDOT, ("NUMLOCK",)): ".",
    (e_ecodes.KEY_KPMINUS, _none): "-",
    (e_ecodes.KEY_KPPLUS, _none): "+",
    (e_ecodes.KEY_KPSLASH, _none): "/",
    (e_ecodes.KEY_L, _none): "i",
    (e_ecodes.KEY_L, _shift): "I",
    (e_ecodes.KEY_LEFTBRACE, _none): "[",
    (e_ecodes.KEY_LEFTBRACE, _shift): "{",
    (e_ecodes.KEY_M, _none): "m",
    (e_ecodes.KEY_M, _shift): "M",
    (e_ecodes.KEY_MINUS, _none): "-",
    (e_ecodes.KEY_MINUS, _shift): "_",
    (e_ecodes.KEY_N, _none): "k",
    (e_ecodes.KEY_N, _shift): "K",
    (e_ecodes.KEY_O, _none): "y",
    (e_ecodes.KEY_O, _shift): "Y",
    (e_ecodes.KEY_P, _none): ";",
    (e_ecodes.KEY_P, _shift): ":",
    (e_ecodes.KEY_Q, _none): "q",
    (e_ecodes.KEY_Q, _shift): "Q",
    (e_ecodes.KEY_R, _none): "p",
    (e_ecodes.KEY_R, _shift): "P",
    (e_ecodes.KEY_RIGHTBRACE, _none): "]",
    (e_ecodes.KEY_RIGHTBRACE, _shift): "}",
    (e_ecodes.KEY_S, _none): "r",
    (e_ecodes.KEY_S, _shift): "R",
    (e_ecodes.KEY_SEMICOLON, _none): "o",
    (e_ecodes.KEY_SEMICOLON, _shift): "O",
    (e_ecodes.KEY_SLASH, _none): "/",
    (e_ecodes.KEY_SLASH, _shift): "?",
    (e_ecodes.KEY_SPACE, _none): " ",
    (e_ecodes.KEY_T, _none): "g",
    (e_ecodes.KEY_T, _shift): "G",
    (e_ecodes.KEY_U, _none): "l",
    (e_ecodes.KEY_U, _shift): "L",
    (e_ecodes.KEY_V, _none): "v",
    (e_ecodes.KEY_V, _shift): "V",
    (e_ecodes.KEY_W, _none): "w",
    (e_ecodes.KEY_W, _shift): "W",
    (e_ecodes.KEY_X, _none): "x",
    (e_ecodes.KEY_X, _shift): "X",
    (e_ecodes.KEY_Y, _none): "j",
    (e_ecodes.KEY_Y, _shift): "J",
    (e_ecodes.KEY_Z, _none): "z",
    (e_ecodes.KEY_Z, _shift): "Z",
}

_ = {
    (e_ecodes.KEY_A, _ralt): "ä",
    (e_ecodes.KEY_A, _shift_ralt): "Ä",
    (e_ecodes.KEY_I, _ralt): "ü",
    (e_ecodes.KEY_I, _shift_ralt): "Ü",
    (e_ecodes.KEY_SEMICOLON, _ralt): "ö",
    (e_ecodes.KEY_SEMICOLON, _shift_ralt): "Ö",
    (e_ecodes.KEY_D, _ralt): "ß",
}
characters.update(_)

characters = {
    keyboard.SimpleEvent(key_code, modifiers, None): character
    for (key_code, modifiers), character in characters.items()
}


encode_simple_events = {}
encode_key_codes = {}

if arguments.windows:
    if arguments.german:
        _ = {
            (e_ecodes.KEY_2, _shift): (e_ecodes.KEY_Q, _ralt),  # @
            (e_ecodes.KEY_3, _shift): (e_ecodes.KEY_BACKSLASH, _none),  # #
            # (e_ecodes.KEY_6, _shift): (e_ecodes.KEY_GRAVE, (" ",)),  # ^
            (e_ecodes.KEY_6, _shift): (e_ecodes.KEY_GRAVE, _none),  # ^
            (e_ecodes.KEY_7, _shift): (e_ecodes.KEY_6, _shift),  # &
            (e_ecodes.KEY_8, _shift): (e_ecodes.KEY_KPASTERISK, _none),  # *
            (e_ecodes.KEY_9, _shift): (e_ecodes.KEY_8, _shift),  # (
            (e_ecodes.KEY_0, _shift): (e_ecodes.KEY_9, _shift),  # )
            (e_ecodes.KEY_MINUS, _none): (e_ecodes.KEY_SLASH, _none),  # -
            (e_ecodes.KEY_MINUS, _shift): (e_ecodes.KEY_SLASH, _shift),  # _
            (e_ecodes.KEY_EQUAL, _none): (e_ecodes.KEY_0, _shift),  # =
            (e_ecodes.KEY_EQUAL, _shift): (e_ecodes.KEY_RIGHTBRACE, _none),  # +
            (e_ecodes.KEY_P, _none): (e_ecodes.KEY_COMMA, _shift),  # ;
            (e_ecodes.KEY_P, _shift): (e_ecodes.KEY_DOT, _shift),  # :
            (e_ecodes.KEY_LEFTBRACE, _none): (e_ecodes.KEY_8, _ralt),  # [
            (e_ecodes.KEY_LEFTBRACE, _shift): (e_ecodes.KEY_7, _ralt),  # {
            (e_ecodes.KEY_RIGHTBRACE, _none): (e_ecodes.KEY_9, _ralt),  # ]
            (e_ecodes.KEY_RIGHTBRACE, _shift): (e_ecodes.KEY_0, _ralt),  # }
            (e_ecodes.KEY_APOSTROPHE, _none): (e_ecodes.KEY_BACKSLASH, _shift),  # '
            (e_ecodes.KEY_APOSTROPHE, _shift): (e_ecodes.KEY_2, _shift),  # "
            # (e_ecodes.KEY_GRAVE, _none): (e_ecodes.KEY_EQUAL, (" ", "SHIFT")),  # `
            (e_ecodes.KEY_GRAVE, _none): (e_ecodes.KEY_EQUAL, _shift),  # `
            (e_ecodes.KEY_GRAVE, _shift): (e_ecodes.KEY_RIGHTBRACE, _ralt),  # ~
            (e_ecodes.KEY_BACKSLASH, _none): (e_ecodes.KEY_MINUS, _ralt),  # \
            (e_ecodes.KEY_BACKSLASH, _shift): (e_ecodes.KEY_102ND, _ralt),  # |
            (e_ecodes.KEY_COMMA, _shift): (e_ecodes.KEY_102ND, _none),  # <
            (e_ecodes.KEY_DOT, _shift): (e_ecodes.KEY_102ND, _shift),  # >
            (e_ecodes.KEY_SLASH, _none): (e_ecodes.KEY_KPSLASH, _none),  # /
            (e_ecodes.KEY_SLASH, _shift): (e_ecodes.KEY_MINUS, _shift),  # ?
            # (e_ecodes.KEY_KPDOT, ("NUMLOCK",)): (e_ecodes.KEY_DOT, _none),  # .
            (e_ecodes.KEY_KPDOT, _none): (e_ecodes.KEY_DOT, _none),  # .
            (e_ecodes.KEY_102ND, _none): (e_ecodes.KEY_MINUS, _ralt),  # \
            (e_ecodes.KEY_102ND, _shift): (e_ecodes.KEY_102ND, _ralt),  # |
        }
        encode_simple_events.update(_)

        _ = {
            e_ecodes.KEY_D: e_ecodes.KEY_S,
            e_ecodes.KEY_E: e_ecodes.KEY_F,
            e_ecodes.KEY_F: e_ecodes.KEY_T,
            e_ecodes.KEY_G: e_ecodes.KEY_D,
            e_ecodes.KEY_I: e_ecodes.KEY_U,
            e_ecodes.KEY_J: e_ecodes.KEY_N,
            e_ecodes.KEY_K: e_ecodes.KEY_E,
            e_ecodes.KEY_L: e_ecodes.KEY_I,
            e_ecodes.KEY_N: e_ecodes.KEY_K,
            e_ecodes.KEY_O: e_ecodes.KEY_Z,
            e_ecodes.KEY_R: e_ecodes.KEY_P,
            e_ecodes.KEY_S: e_ecodes.KEY_R,
            e_ecodes.KEY_SEMICOLON: e_ecodes.KEY_O,
            e_ecodes.KEY_T: e_ecodes.KEY_G,
            e_ecodes.KEY_U: e_ecodes.KEY_L,
            e_ecodes.KEY_Y: e_ecodes.KEY_J,
            e_ecodes.KEY_Z: e_ecodes.KEY_Y,
        }
        encode_key_codes.update(_)

    else:
        _ = {
            (e_ecodes.KEY_P, _none): (e_ecodes.KEY_SEMICOLON, _none),  # ;
            (e_ecodes.KEY_P, _shift): (e_ecodes.KEY_SEMICOLON, _shift),  # :
        }
        encode_simple_events.update(_)

        _ = {
            e_ecodes.KEY_D: e_ecodes.KEY_S,
            e_ecodes.KEY_E: e_ecodes.KEY_F,
            e_ecodes.KEY_F: e_ecodes.KEY_T,
            e_ecodes.KEY_G: e_ecodes.KEY_D,
            e_ecodes.KEY_I: e_ecodes.KEY_U,
            e_ecodes.KEY_J: e_ecodes.KEY_N,
            e_ecodes.KEY_K: e_ecodes.KEY_E,
            e_ecodes.KEY_L: e_ecodes.KEY_I,
            e_ecodes.KEY_N: e_ecodes.KEY_K,
            e_ecodes.KEY_O: e_ecodes.KEY_Y,
            e_ecodes.KEY_R: e_ecodes.KEY_P,
            e_ecodes.KEY_S: e_ecodes.KEY_R,
            e_ecodes.KEY_SEMICOLON: e_ecodes.KEY_O,
            e_ecodes.KEY_T: e_ecodes.KEY_G,
            e_ecodes.KEY_U: e_ecodes.KEY_L,
            e_ecodes.KEY_Y: e_ecodes.KEY_J,
        }
        encode_key_codes.update(_)

if arguments.german:
    _ = {
        (e_ecodes.KEY_A, _ralt): (e_ecodes.KEY_APOSTROPHE, _none),  # ä
        (e_ecodes.KEY_A, _shift_ralt): (e_ecodes.KEY_APOSTROPHE, _shift),  # Ä
        (e_ecodes.KEY_SEMICOLON, _ralt): (e_ecodes.KEY_SEMICOLON, _none),  # ö
        (e_ecodes.KEY_SEMICOLON, _shift_ralt): (e_ecodes.KEY_SEMICOLON, _shift),  # Ö
        (e_ecodes.KEY_I, _ralt): (e_ecodes.KEY_LEFTBRACE, _none),  # ü
        (e_ecodes.KEY_I, _shift_ralt): (e_ecodes.KEY_LEFTBRACE, _shift),  # Ü
        (e_ecodes.KEY_D, _ralt): (e_ecodes.KEY_MINUS, _none),  # ß
    }
    encode_simple_events.update(_)

else:
    _ = {
        (e_ecodes.KEY_A, _ralt): (e_ecodes.KEY_Q, _ralt),  # ä
        (e_ecodes.KEY_A, _shift_ralt): (e_ecodes.KEY_Q, _shift_ralt),  # Ä
        (e_ecodes.KEY_SEMICOLON, _ralt): (e_ecodes.KEY_P, _ralt),  # ö
        (e_ecodes.KEY_SEMICOLON, _shift_ralt): (e_ecodes.KEY_P, _shift_ralt),  # Ö
        (e_ecodes.KEY_I, _ralt): (e_ecodes.KEY_O, _ralt),  # ü
        (e_ecodes.KEY_I, _shift_ralt): (e_ecodes.KEY_O, _shift_ralt),  # Ü
    }
    encode_simple_events.update(_)


def _get_simple_event(tuple):
    key_code, modifiers = tuple
    return keyboard.SimpleEvent(key_code, modifiers, None)


encode_simple_events = {
    _get_simple_event(from_tuple): _get_simple_event(to_tuple)
    for from_tuple, to_tuple in encode_simple_events.items()
}


_ = arguments.windows and arguments.german
_ = [e_ecodes.KEY_LEFTCTRL, e_ecodes.KEY_LEFTALT] if _ else [e_ecodes.KEY_RIGHTALT]
_ = [e_ecodes.KEY_LEFTSHIFT, e_ecodes.KEY_RIGHTSHIFT] + _
stick_modifiers = keyboard.StickModifiers(_)


_ = arguments.windows
with_master_password = k_mpw.WithMasterPassword(
    "Andreas Rappold",
    [pathlib.Path(PATH, "password"), pathlib.Path(PATH, "work password")],
    characters,
    version_path=pathlib.Path(PATH, "versions"),
    mpw_parts=(
        ["podman", "run", "-i", "--rm", "docker://miripii/mpw-cli"] if _ else ["mpw"]
    ),
)


def substitute_simple_events():
    if len(encode_simple_events) == 0:
        return

    return keyboard.SubstituteSimpleEvents(
        encode_simple_events,
        key_code_dictionary=None if len(encode_key_codes) == 0 else encode_key_codes,
    )


def substitute_key_codes():
    if not (len(encode_simple_events) == 0 and len(encode_key_codes) != 0):
        return

    return keyboard.SubstituteKeyCodes(encode_key_codes)


transformations = [
    # keyboard.CategorizeEvents("in"),
    (k_windows.FixWindowsIn() if arguments.windows else None),
    keyboard.Pausable(e_ecodes.KEY_RIGHTCTRL, ungrab=not arguments.windows),
    keyboard.RetimeRepeats(),
    keyboard.SubstituteKeyCodes(key_codes),
    # keyboard.ModifierAsKey({e_ecodes.KEY_LEFTSHIFT: e_ecodes.KEY_SPACE}),
    stick_modifiers,
    keyboard.ToSimpleEvents(),
    with_master_password,
    substitute_simple_events(),
    # keyboard.PrintSimpleEvents(),
    keyboard.ToEvents(),
    substitute_key_codes(),
    keyboard.DelayEvents(),
    k_windows.FixWindowsOut(e_ecodes.KEY_F13) if arguments.windows else None,
    # keyboard.CategorizeEvents("out"),
]

_ = [transformation for transformation in transformations if transformation is not None]
keyboard.main(arguments.device, _)
