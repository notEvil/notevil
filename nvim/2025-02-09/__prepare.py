import pylnk3
import pathlib
import subprocess


PATH = pathlib.Path(__file__).parent


init_path = pathlib.Path(PATH, "init.lua")

# INFO nvim-qt.exe doesn't install anything
_ = [
    r"C:\Program Files\Neovim\bin\nvim.exe",
    "-u",
    str(init_path),
    "+call dein#install()",
]
assert subprocess.Popen(_).wait() == 0

pylnk3.for_file(
    str(pathlib.Path(r"C:\Program Files\Neovim\bin\nvim-qt.exe")),
    arguments=subprocess.list2cmdline(["--", "-u", str(init_path)]),
    lnk_name=str(pathlib.Path(pathlib.Path.home(), "Desktop", "nvim.lnk")),
)
