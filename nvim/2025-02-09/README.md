In the past I aimed for easy install on Windows, see [https://gitlab.com/notEvil/zero](https://gitlab.com/notEvil/zero).
The main reason was that Windows is still very popular amongst businesses as the primary OS.
However, easy install wasn't easy to provide and I actually never used it.
