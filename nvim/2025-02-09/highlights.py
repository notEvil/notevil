CONSTANT = None  # @constant
_ = (None, False)  # @constant.builtin


class C1:
    def __init__(self):
        pass


_ = C1()  # @constructor


def f1():  # @function
    pass


_ = abs(0)  # @function.builtin
_ = f1()  # @function.call


class C3:
    def method(self):  # @function.method
        pass


_ = C3().method()  # @function.method.call


import asyncio as _  # @keyword.import


_ = "string"  # @string
_ = "\n"  # @string.escape


class Type:  # @type
    pass


class C2:
    field = None  # @variable.member


def f2(parameter):  # @variable.parameter
    _ = parameter
