-- break dependency circles

local m_lua
local m_repeat_function
local m_shared

function calling(function_)
  local _function
  return function(...)
    if _function == nil then
      _function = m_repeat_function.calling(function_)
    end
    return _function(...)
  end
end

function feeding(function_)
  local _function
  return function(...)
    if _function == nil then
      _function = m_repeat_function.feeding(function_)
    end
    return _function(...)
  end
end
-- break dependency circle


function get_hunk(number)  -- :diffget from {number}th buffer
  for _, window in ipairs(vim.api.nvim_list_wins()) do
    if vim.api.nvim_win_get_option(window, 'diff') then
      if number == 1 then
        m_shared.feedkeys(':diffget ' .. vim.api.nvim_win_get_buf(window) .. '<cr>')
        return
      else
        number = number - 1
      end
    end
  end
end


function undo_change()  -- in unified diff format
  m_shared.feedkeys('<esc>')
  local begin_line = m_shared.get_position('<')
  local end_line = m_shared.get_position('>')
  local lines = vim.api.nvim_buf_get_lines(0, begin_line - 1, end_line, true)
  local new_lines = {}
  for _, line in ipairs(lines) do
    if line:find('^+') ~= nil then
    elseif line:find('^-') then
      table.insert(new_lines, ' ' .. line:sub(2))
    else
      table.insert(new_lines, line)
    end
  end
  vim.api.nvim_buf_set_lines(0, begin_line - 1, end_line, true, new_lines)
end


-- plugins

-- https://github.com/folke/lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)


function keymap_filter(item, context)  -- show if keymap exists
  for _, keymap in ipairs(vim.api.nvim_buf_get_keymap(context.buf, context.mode)) do
    if keymap.lhs == item.keys then
      return true
    end
  end
  return false
end

function get_mode_filter(modes)  -- show if in mode
  if type(modes) ~= 'table' then
    modes = {modes}
  end
  return function(item, context)
    return m_lua.get_key(modes, context.mode) or (m_lua.get_key(modes, 'v') and require('legendary.toolbox').is_visual_mode(mode))
  end
end

local _n = get_mode_filter('n')
local _v = get_mode_filter('v')


-- patch treesitter

-- git restore
vim.api.nvim_create_autocmd('User', {pattern={'LazyInstallPre', 'LazyUpdatePre'}, callback=function()
  vim.api.nvim_exec2([[
python <<EOF
import pathlib
import subprocess

path = pathlib.Path(']] .. vim.fn.stdpath('data') .. [[/lazy/nvim-treesitter')
if path.exists():
    assert subprocess.Popen(['git', 'restore', '.'], cwd=path).wait() == 0
EOF
]], {})
end})

-- patch
vim.api.nvim_create_autocmd('User', {pattern={'LazyInstall', 'LazyUpdate'}, callback=function()
  vim.api.nvim_exec2([[
python <<EOF
import subprocess

process = subprocess.Popen(['patch', '-s', '-p1'], stdin=subprocess.PIPE, cwd=']] .. vim.fn.stdpath('data') .. [[/lazy/nvim-treesitter')

process.communicate('''
diff --git a/queries/python/highlights.scm b/queries/python/highlights.scm
index 0c7147b5..e73a2298 100644
--- a/queries/python/highlights.scm
+++ b/queries/python/highlights.scm
@@ -370,7 +370,7 @@
 
 ; Class definitions
 (class_definition
-  name: (identifier) @type)
+  name: (identifier) @function)
 
 (class_definition
   body:
@@ -401,31 +401,6 @@
               (identifier) @variable.member)))))
   (#lua-match? @variable.member "^[%l_].*$"))
 
-((class_definition
-  (block
-    (function_definition
-      name: (identifier) @constructor)))
-  (#any-of? @constructor "__new__" "__init__"))
-
-((identifier) @type.builtin
-  ; format-ignore
-  (#any-of? @type.builtin
-    ; https://docs.python.org/3/library/exceptions.html
-    "BaseException" "Exception" "ArithmeticError" "BufferError" "LookupError" "AssertionError" "AttributeError"
-    "EOFError" "FloatingPointError" "GeneratorExit" "ImportError" "ModuleNotFoundError" "IndexError" "KeyError"
-    "KeyboardInterrupt" "MemoryError" "NameError" "NotImplementedError" "OSError" "OverflowError" "RecursionError"
-    "ReferenceError" "RuntimeError" "StopIteration" "StopAsyncIteration" "SyntaxError" "IndentationError" "TabError"
-    "SystemError" "SystemExit" "TypeError" "UnboundLocalError" "UnicodeError" "UnicodeEncodeError" "UnicodeDecodeError"
-    "UnicodeTranslateError" "ValueError" "ZeroDivisionError" "EnvironmentError" "IOError" "WindowsError"
-    "BlockingIOError" "ChildProcessError" "ConnectionError" "BrokenPipeError" "ConnectionAbortedError"
-    "ConnectionRefusedError" "ConnectionResetError" "FileExistsError" "FileNotFoundError" "InterruptedError"
-    "IsADirectoryError" "NotADirectoryError" "PermissionError" "ProcessLookupError" "TimeoutError" "Warning"
-    "UserWarning" "DeprecationWarning" "PendingDeprecationWarning" "SyntaxWarning" "RuntimeWarning"
-    "FutureWarning" "ImportWarning" "UnicodeWarning" "BytesWarning" "ResourceWarning"
-    ; https://docs.python.org/3/library/stdtypes.html
-    "bool" "int" "float" "complex" "list" "tuple" "range" "str"
-    "bytes" "bytearray" "memoryview" "set" "frozenset" "dict" "type" "object"))
-
 ; Regex from the `re` module
 (call
   function:
'''[1:-1].encode())
assert process.wait() == 0
EOF
]], {})
end})
-- patch treesitter


require('lazy').setup({
  {
    'stevearc/aerial.nvim',
    opts={keymaps={}, autojump=true},
  },
  {
    'psf/black',
    branch='stable',
  },
  'isobit/vim-caddyfile',
  {
    'neoclide/coc.nvim',
    branch='master',
    build={'npm ci', ':CocInstall coc-pyright'},
  },
  {
    'notEvil/code_collapse',
    url='https://gitlab.com/notEvil/code_collapse',
  },
  {
    'numToStr/Comment.nvim',
    config=true,
  },
  {
    'mfussenegger/nvim-dap-python',
    dependencies={'mfussenegger/nvim-dap'},
    config=function() require('dap-python').setup('python') end,
  },
  {
    'rcarriga/nvim-dap-ui',
    dependencies={'mfussenegger/nvim-dap'},
    opts={mappings={
      edit='i',
      expand='<tab>',
      open='<cr>',
      remove='d',
      repl={},
      toggle='x',
    }},
  },
  {
    'stevearc/dressing.nvim',
    config=true,
  },
  {
    'ellisonleao/gruvbox.nvim',
    priority=1000,
  },
  {
    'mrjones2014/legendary.nvim',
    version='*',
    dependencies={'kkharji/sqlite.lua'},
    opts={
      keymaps={
        {description='Step into', [1]='<f9>', mode='n'},
        {description='Step over', [1]='<f10>', mode='n'},
        {description='Step out', [1]='<f11>', mode='n'},
        {description='Continue execution', [1]='<f12>', mode='n'},
      },
      funcs={
        {description='Undo change', [1]=calling(undo_change), filters={_v}},
        {description='Get hunk from 1st file', [1]=function() get_hunk(1) end, filters={_n}},
        {description='Get hunk from 2nd file', [1]=function() get_hunk(2) end, filters={_n}},
        {description='Get hunk from 3rd file', [1]=function() get_hunk(3) end, filters={_n}},
        {description='Collapse code', [1]=feeding(':CollapseCode<cr>'), filters={_v}},
        {description='Collapse file', [1]=feeding(':CollapseFile<cr>'), filters={_n}},
        {description='Collapse file with git diff', [1]=feeding(':CollapseFile git=1<cr>'), filters={_n}},
        {description='Expand code', [1]=feeding(':CollapseCode expand=1<cr>'), filters={_v}},
        {description='Expand file', [1]=feeding(':CollapseFile expand=1<cr>'), filters={_n}},
        {description='Expand file with git diff', [1]=feeding(':CollapseFile expand=1 git=1<cr>'), filters={_n}},
        {description='Toggle outline', [1]=feeding(':AerialToggle!<cr>'), filters={_n}},
        {description='Format code', [1]=function() m_shared.feedkeys(':Black<cr>') end, filters={_n}},
        {description='Toggle comment', [1]=feeding('<leader>ct', true), filters={get_mode_filter({'n', 'v'})}},
        {description='Toggle debugger', [1]=calling(function() require('dapui').toggle() end), filters={_n}},
        {description='Toggle tree', [1]=feeding(':Neotree toggle<cr>'), filters={_n}},
        {description='Use filesystem', [1]=function() m_shared.feedkeys(':Neotree source=filesystem<cr>') end, filters={_n}},
        {description='Use buffers', [1]=function() m_shared.feedkeys(':Neotree source=buffers<cr>') end, filters={_n}},
        {description='Use Git status', [1]=function() m_shared.feedkeys(':Neotree source=git_status<cr>') end, filters={_n}},
        {description='Find file', [1]=function() require('telescope').extensions.smart_open.smart_open() end, filters={_n}},
        {description='Change Surround', [1]=function() m_shared.feedkeys('<leader>sc', true) end, filters={_n}},
        {description='Delete Surround', [1]=function() m_shared.feedkeys('<leader>sd', true) end, filters={_n}},
        {description='Insert Surround', [1]=function() m_shared.feedkeys('<leader>si', true) end, filters={_v}},

        {description='git: Commit', [1]=function() m_shared.feedkeys(';_gpdpC', true) end, filters={_n, keymap_filter}},
        {description='git: Reset all files', [1]=function() m_shared.feedkeys(';_gpdpR', true) end, filters={_n, keymap_filter}},
        {description='git: Reset all hunks', [1]=function() m_shared.feedkeys(';_gdpr', true) end, filters={_n, keymap_filter}},
        {description='git: Reset file', [1]=feeding(';_gpdpr', true), filters={_n, keymap_filter}},
        {description='git: Save commit', [1]=function() m_shared.feedkeys(';_gpcpS', true) end, filters={_n, keymap_filter}},  -- TODO repeat?
        {description='git: Stage all files', [1]=function() m_shared.feedkeys(';_gpdpS', true) end, filters={_n, keymap_filter}},
        {description='git: Stage all hunks', [1]=function() m_shared.feedkeys(';_gdpS', true) end, filters={_n, keymap_filter}},
        {description='git: Stage file', [1]=feeding(';_gpdps', true), filters={_n, keymap_filter}},
        {description='git: Stage hunk', [1]=feeding(';_gdps', true), filters={_n, keymap_filter}},
        {description='git: Stage hunk', [1]=feeding(';_gpdpgs', true), filters={_n, keymap_filter}},
        {description='git: Toggle view', [1]=feeding(';_gdpt', true), filters={_n, keymap_filter}},
        {description='git: Unstage all files', [1]=function() m_shared.feedkeys(';_gpdpU', true) end, filters={_n, keymap_filter}},
        {description='git: Unstage all hunks', [1]=function() m_shared.feedkeys(';_gdpU', true) end, filters={_n, keymap_filter}},
        {description='git: Unstage file', [1]=feeding(';_gpdpu', true), filters={_n, keymap_filter}},
        {description='git: Unstage hunk', [1]=feeding(';_gdpu', true), filters={_n, keymap_filter}},
        {description='git: Unstage hunk', [1]=feeding(';_gpdpgu', true), filters={_n, keymap_filter}},

        {description='git: Commit', [1]=function() require('vgit').project_commit_preview() end, filters={_n}},
        {description='git: Edit commit', [1]=function() require('vgit').project_diff_preview() end, filters={_n}},
        {description='git: Edit hunks', [1]=function() require('vgit').buffer_diff_preview() end, filters={_n}},
        {description='git: Preview hunk', [1]=calling(function() require('vgit').buffer_hunk_preview() end), filters={_n}},
        {description='git: Preview line blame', [1]=calling(function() require('vgit').buffer_blame_preview() end), filters={_n}},
        {description='git: Preview staged hunk', [1]=calling(function() require('vgit').buffer_hunk_staged_preview() end), filters={_n}},
        {description='git: Quickfix hunks', [1]=function() require('vgit').project_hunks_qf() end, filters={_n}},
        {description='git: Reset all files', [1]=function() require('vgit').project_reset_all() end, filters={_n}},
        {description='git: Reset all hunks', [1]=function() require('vgit').buffer_reset() end, filters={_n}},
        {description='git: Reset hunk', [1]=calling(function() require('vgit').buffer_hunk_reset() end), filters={_n}},
        {description='git: Stage all files', [1]=function() require('vgit').project_stage_all() end, filters={_n}},
        {description='git: Stage all hunks', [1]=function() require('vgit').buffer_stage() end, filters={_n}},
        {description='git: Stage hunk', [1]=calling(function() require('vgit').buffer_hunk_stage() end), filters={_n}},
        {description='git: Toggle authorship code lens', [1]=calling(function() require('vgit').toggle_authorship_code_lens() end), filters={_n}},
        {description='git: Toggle blame', [1]=calling(function() require('vgit').toggle_live_blame() end), filters={_n}},
        {description='git: Toggle debug logging', [1]=calling(function() require('vgit').toggle_tracing() end), filters={_n}},
        {description='git: Toggle diff preference', [1]=calling(function() require('vgit').toggle_diff_preference() end), filters={_n}},
        {description='git: Toggle gutter signs', [1]=calling(function() require('vgit').toggle_live_gutter() end), filters={_n}},
        {description='git: Unstage all files', [1]=function() require('vgit').project_unstage_all() end, filters={_n}},
        {description='git: Unstage all hunks', [1]=function() require('vgit').buffer_unstage() end, filters={_n}},
        {description='git: View blame', [1]=function() require('vgit').buffer_gutter_blame_preview() end, filters={_n}},
        {description='git: View history', [1]=function() require('vgit').buffer_history_preview() end, filters={_n}},
        {description='git: View hunks', [1]=function() require('vgit').project_hunks_preview() end, filters={_n}},
        {description='git: View log', [1]=function() require('vgit').project_logs_preview() end, filters={_n}},
        {description='git: View staged hunks', [1]=function() require('vgit').project_hunks_staged_preview() end, filters={_n}},
        {description='git: View stash', [1]=function() require('vgit').project_stash_preview() end, filters={_n}},
      },
    },
  },
  {
    'notEvil/mynvim',
    url='https://gitlab.com/notEvil/mynvim',
    dependencies={'notEvil/vim-easymotion'},
  },
  {
    'notEvil/neo-tree.nvim',
    branch='edit_directory',
    dependencies={'nvim-lua/plenary.nvim', 'MunifTanjim/nui.nvim'},
    opts={
      use_default_mappings=false,
      filesystem={
        hijack_netrw_behavior='disabled',
        filtered_items={visible=true},
        follow_current_file={enabled=true},
        use_libuv_file_watcher=true,
      },
      window={mappings={
        ['<f5>']='refresh',
        ['<tab>']='toggle_node',
        ['<bs>']='close_node',
        ['<cr>']=function(state) require('neo-tree.sources.common.commands').open(state, 'edit') end,
      }},
    },
  },
  {
    'stevearc/oil.nvim',
    opts={
      use_default_keymaps=false,
      view_options={show_hidden=true},
      keymaps={
        ['<f5>']='actions.refresh',
        ['<cr>']='actions.select',
        ['<bs>']='actions.parent',
      },
    },
  },
  {
    'danielfalk/smart-open.nvim',
    branch='0.2.x',
    dependencies={
      'kkharji/sqlite.lua',
      'nvim-telescope/telescope.nvim',
      'nvim-telescope/telescope-fzy-native.nvim',
    },
    init=function() require('telescope').load_extension('smart_open') end,
  },
  'tpope/vim-surround',
  {
    'nvim-telescope/telescope.nvim',
    dependencies={'nvim-lua/plenary.nvim'},
    opts={
      defaults={
        layout_config={prompt_position='top'},
        sorting_strategy='ascending',
        default_mappings={i={
          ['<a-n>']='move_selection_next',
          ['<a-e>']='move_selection_previous',
          ['<cr>']='select_default',
          ['<esc>']='close',
        }},
      },
    },
  },
  {
    'nvim-treesitter/nvim-treesitter',
    build=':TSUpdate',
    config=function()
      require('nvim-treesitter.configs').setup({
        ensure_installed={
          'd',
          'javascript',
          'markdown',
          'markdown_inline',
          'norg',
          'python',
          'typescript',
        },
        highlight={
          enable=true,
          additional_vim_regex_highlighting=false,
        },
        incremental_selection={
          enable=true,
          keymaps={init_selection=false, node_incremental=false, scope_incremental=false, node_decremental=false},
        },
      })
    end,
  },
  {
    'tanvirtin/vgit.nvim',
    dependencies={'nvim-lua/plenary.nvim'},
    opts={
      settings={
        live_blame={enabled=false},
        scene={diff_preference='split'},
        diff_preview={keymaps={
          reset = '<leader>_gdpr',
          buffer_stage = '<leader>_gdpS',
          buffer_unstage = '<leader>_gdpU',
          buffer_hunk_stage = '<leader>_gdps',
          buffer_hunk_unstage = '<leader>_gdpu',
          toggle_view = '<leader>_gdpt',
        }},
        project_diff_preview={keymaps={
          commit = '<leader>_gpdpC',
          buffer_stage = '<leader>_gpdps',
          buffer_unstage = '<leader>_gpdpu',
          buffer_hunk_stage = '<leader>_gpdpgs',
          buffer_hunk_unstage = '<leader>_gpdpgu',
          buffer_reset = '<leader>_gpdpr',
          stage_all = '<leader>_gpdpS',
          unstage_all = '<leader>_gpdpU',
          reset_all = '<leader>_gpdpR',
        }},
        project_commit_preview={keymaps={
          save = '<leader>_gpcpS',
        }},
      },
    },
  },
  {
    'nvim-tree/nvim-web-devicons',
    config=true,
  },
})
-- plugins


local mynvim = require('mynvim')
m_lua = require('mynvim.lua')
m_repeat_function = require('mynvim.repeat_function')
m_shared = require('mynvim.shared')

mynvim.initialize(true)


-- default options
vim.g.mapleader = ';'
vim.opt.conceallevel = 2  -- enable conceil
vim.opt.diffopt:append('linematch:60')  -- align lines in diffs
vim.opt.expandtab = true  -- use spaces instead of tabs
vim.opt.ignorecase = true  -- in search
vim.opt.number = true  -- show line numbers
vim.opt.shiftwidth = 2  -- step size of horizontal shift
vim.opt.showcmd = false  -- don't show keys in status line
vim.opt.smartcase = true  -- don't ignore case in search if pattern contains upper case
vim.opt.sidescrolloff = 20  -- scroll horizontally at end of line
vim.opt.wildcharm = string.byte(m_shared.replace_termcodes('<tab>'))  -- required for Tab to work in command mode
vim.opt.wrap = false  -- don't wrap lines
vim.opt.wrapscan = false  -- don't wrap search at end of document

-- default colors
m_shared.execute_command('colorscheme', {'gruvbox'})

-- default keymaps
-- "one down"
mynvim.map('<a-n>', '<tab>', 'c')
mynvim.map('<a-n>', function()  -- start autocomplete or next suggestion
  if vim.fn['coc#pum#visible']() == 0 then
    m_shared.feedkeys(vim.fn['coc#refresh']())
  else
    m_shared.feedkeys(vim.fn['coc#pum#next'](true))
  end
end, 'i')
-- "one up"
mynvim.map('<a-e>', '<s-tab>', 'c')
mynvim.map('<a-e>', function()  -- start autocomplete or previous suggestion
  if vim.fn['coc#pum#visible']() == 0 then
    m_shared.feedkeys(vim.fn['coc#refresh']())
  else
    m_shared.feedkeys(vim.fn['coc#pum#prev'](true))
  end
end, 'i')
-- "cancel autocomplete"
mynvim.map('<a-bs>', function()  -- cancel autocomplete or pass on
  if vim.fn['coc#pum#visible']() == 0 then
     m_shared.feedkeys('<a-bs>')
   else
     m_shared.feedkeys(vim.fn['coc#pum#cancel']())
   end
 end, 'i')
mynvim.map('<esc>', '<c-\\><c-n>', 't')
-- "switch to adjacent window"
mynvim.map('<a-E>', '<c-w>k', 'n')
mynvim.map('<a-H>', '<c-w>h', 'n')
mynvim.map('<a-I>', '<c-w>l', 'n')
mynvim.map('<a-N>', '<c-w>j', 'n')
-- diff
mynvim.map('<leader>dP', m_repeat_function.dot('dp'), 'n')  -- "Diff put"
mynvim.map('<leader>dp', m_repeat_function.dot('do'), 'n')  -- "Diff obtain"  -- TODO dot repeatable?
mynvim.map('<leader>du', feeding(':dif<cr>'), 'n')  -- "update diff"  -- TODO add to legendary
-- misc
mynvim.map('<a-o>', '<esc>', {'c', 'i', 'n', 't', 'v'}, {remap=true})  -- "exit mode"
mynvim.map('<leader>hd', ':nohlsearch<cr>', 'n')  -- "hide highlights"
mynvim.map('<leader>w=', '<c-w>=', 'n')  -- "All windows same size"
mynvim.map('<space>', function() m_shared.feedkeys(m_lua.if_(vim.bo.filetype == 'python', ':Black<cr>', ':CocCommand editor.action.formatDocument<cr>')) end, 'n')  -- "autoformat document"

-- initialize
-- diff
vim.api.nvim_create_autocmd('VimEnter', {callback=function()  -- set first buffer readonly, switch to second buffer and move to first hunk
  local count = 0
  for _, window in ipairs(vim.api.nvim_list_wins()) do
    if vim.api.nvim_win_get_option(window, 'diff') then
      count = count + 1
      if count == 2 then
        vim.api.nvim_set_current_win(window)
        m_shared.feedkeys('sh', true)
      else
        vim.api.nvim_buf_set_option(vim.api.nvim_win_get_buf(window), 'readonly', true)
      end
    end
  end
end})

-- plugins

-- aerial
mynvim.map('<leader>ot', feeding(':AerialToggle!<cr>'), 'n')  -- "Toggle outline"

-- black
vim.g.black_preview = 1  -- use recently added features
vim.g.black_skip_magic_trailing_comma = 1  -- disable forced line breaks if collection ends with comma

-- coc.nvim
vim.fn['coc#config']('suggest.noselect', 1)  -- start at first suggestion
mynvim.map('<Down>', function() vim.fn['coc#float#scroll'](1, 1) end, {'n', 'i'})  -- "scroll down one line"
mynvim.map('<PageDown>', function() vim.fn['coc#float#scroll'](1) end, {'n', 'i'})  -- "scroll down one page"
mynvim.map('<PageUp>', function() vim.fn['coc#float#scroll'](0) end, {'n', 'i'})  -- "scroll up one page"
mynvim.map('<Up>', function() vim.fn['coc#float#scroll'](0, 1) end, {'n', 'i'})  -- "scroll up one line"
mynvim.map('<leader>ds', function() vim.fn.CocAction('doHover') end, 'n')  -- "show documentation"
mynvim.map('gd', function() vim.fn.CocAction('jumpDefinition') end, 'n')  -- "goto definition"
mynvim.map('gr', function() vim.fn.CocAction('jumpUsed') end, 'n')  -- "goto references"
mynvim.map('<leader>rf', function()  -- "fix references"
  local locations = vim.fn.CocAction('references', true)
  local line, column = m_shared.get_position()
  local list = {}
  for _, location in ipairs(locations) do
    assert(location.uri:sub(1, 7) == 'file://')
    table.insert(list, {
      filename=location.uri:gsub('%%(%x%x)', function(a) return string.char(tonumber(a, 16)) end):sub(8),
      lnum=location.range.start.line + 1,
      col=location.range.start.character + 1,
    })
  end
  table.sort(list, function(a, b)
    for _, name in ipairs({'filename', 'lnum', 'col'}) do
      local a_object = a[name]
      local b_object = b[name]
      if a_object < b_object then
        return true
      end
      if b_object < a_object then
        return false
      end
    end
  end)
  vim.fn.setqflist(list)
  m_shared.execute_command('cwindow')
end, 'n')
mynvim.map_move('si', 'Si', function(backward) m_shared.feedkeys(m_lua.if_(backward, '<Plug>(coc-diagnostic-prev)', '<Plug>(coc-diagnostic-next)')) end, 'n')  -- "move to start of issue"

-- code_collapse
mynvim.map('<leader>cc', feeding(':CollapseFile git=1<cr>'), 'n')  -- "Collapse file with git diff"
mynvim.map('<leader>cc', feeding(':CollapseCode<cr>'), 'v')  -- "Collapse code"
mynvim.map('<leader>cC', feeding(':CollapseFile git=1 expand=1<cr>'), 'n')  -- "Expand file with git diff"
mynvim.map('<leader>cC', feeding(':CollapseCode expand=1<cr>'), 'v')  -- "Expand code"

-- Comment
mynvim.map('<leader>ct', feeding('<Plug>(comment_toggle_linewise_current)'), 'n')  -- "Toggle comment"
mynvim.map('<leader>ct', feeding('<Plug>(comment_toggle_linewise_visual)'), 'v')  -- "Toggle comment"

-- dap
mynvim.map('<f9>', function() require('dap').step_into() end, 'n')  -- "Step into"
mynvim.map('<f10>', function() require('dap').step_over() end, 'n')  -- "Step over"
mynvim.map('<f11>', function() require('dap').step_out() end, 'n')  -- "Step out"
mynvim.map('<f12>', function() require('dap').continue() end, 'n')  -- "Continue execution"
mynvim.map('<leader>dt', calling(function() require('dapui').toggle() end), 'n')  -- "Toggle debugger"

-- vim-easymotion
vim.g.EasyMotion_keys = 'esiroalpufywqmcxznt'
vim.g.EasyMotion_smartcase = 1  -- case sensitive if pattern contains upper case

-- grep
vim.cmd([[
" https://gist.github.com/romainl/56f0c28ef953ffc157f36cc495947ab3
set grepprg=rg\ --vimgrep\ -S\ --sort\ path

function! Grep(...)
  " return system(join([&grepprg] + [expandcmd(join(a:000, ' '))], ' '))
  " return system(&grepprg .. ' ' .. join(a:000, ' '))
  let args = []
  let i = index(a:000, '--')
  if i != -1
    call extend(args, a:000[:i])
  endif
  let i += 1
  if a:0 - i == 0
    return ''
  endif
  call add(args, join(a:000[i:(a:0 - i == 1 ? i : -2)], ' '))
  if a:0 - i != 1
    call add(args, expand(a:000[-1]))
  endif
  " echom &grepprg .. ' ' .. join(map(args, {_, arg -> shellescape(arg)}), ' ')
  return system(&grepprg .. ' ' .. join(map(args, {_, arg -> shellescape(arg)}), ' '))
endfunction

command! -nargs=+ -complete=file_in_path -bar Grep  cgetexpr Grep(<f-args>)

cnoreabbrev <expr> grep  (getcmdtype() ==# ':' && getcmdline() ==# 'grep')  ? 'Grep'  : 'grep'

autocmd QuickFixCmdPost cgetexpr cwindow
]])

-- legendary
function _repeat_legendary() require('legendary').repeat_previous() end
mynvim.map('<a-k>', function() m_repeat_function.call(  -- "open menu"
  function() require('legendary').find() end, {c=_repeat_legendary, i=_repeat_legendary, t=_repeat_legendary}
) end, {'c', 'i', 't'})
mynvim.map('k', function() m_repeat_function.call(  -- "open menu"
  function() require('legendary').find() end, {n=_repeat_legendary}
) end, 'n')
mynvim.map('k', function() m_repeat_function.call(  -- "open menu"
  function() require('legendary').find() end, {v=_repeat_legendary, V=_repeat_legendary, [m_shared.CTRL_V]=_repeat_legendary}
) end, 'v')

-- neo-tree
mynvim.map('<leader>tt', feeding(':Neotree toggle<cr>'), 'n')  -- "Toggle tree"

-- Python Execute
vim.g.pe_auto_scope = 1
vim.g.pe_auto_start = 1
mynvim.map('<a-cr>', '<c-o>:ExecutePython i<cr>', 'i')
mynvim.map('<a-cr>', ':ExecutePython n<cr>', 'n')
mynvim.map('<a-cr>', ':<c-u>ExecutePython v<cr>', 'v')

-- smart-open
mynvim.map('gf', function() require('telescope').extensions.smart_open.smart_open() end, 'n')  -- "goto file"

-- vim-surround
vim.g.surround_no_mappings = 1  -- disable default mappings
mynvim.map('<leader>sc', feeding('<Plug>Csurround', true), 'n')  -- "Change Surround"
mynvim.map('<leader>sd', feeding('<Plug>Dsurround', true), 'n')  -- "Delete Surround"
mynvim.map('<leader>si', feeding('<Plug>VSurround', true), 'v')  -- "Insert Surround"

-- nvim-treesitter
mynvim.map('[', function()  -- "select node under cursor"
  local line, column = m_shared.get_position()
  m_shared.set_position(line, column, '`')
  require('nvim-treesitter.incremental_selection').init_selection()
end, 'n')
mynvim.map('[', function()  -- "select parent node"
  m_shared.feedkeys('<esc>')
  require('nvim-treesitter.incremental_selection').node_incremental()
end, 'v')
mynvim.map('{', function()  -- "select child node"
  m_shared.feedkeys('<esc>')
  require('nvim-treesitter.incremental_selection').node_decremental()
  if vim.api.nvim_get_mode()['mode'] == 'n' then
    m_shared.feedkeys('``')
  end
end, 'v')
-- initialize
vim.api.nvim_create_autocmd('FileType', {pattern='python', callback=function()  -- adjust colors
  local _ = function(...) m_shared.execute_command('highlight', {'link', ...}) end
  _('@constant', '@variable')
  _('@constant.builtin', 'Number')
  _('@constructor', 'GruvboxGreen')
  _('@function', 'GruvboxYellow')
  _('@function.builtin', 'GruvboxGreen')
  _('@function.call', 'GruvboxGreen')
  _('@function.method', 'GruvboxYellow')
  _('@function.method.call', 'GruvboxGreen')
  _('@keyword.import', 'GruvboxRed')
  _('@string', 'GruvboxAqua')
  _('@string.escape', 'Number')
  _('@type', '@variable')
  _('@variable.member', '@variable')
  _('@variable.parameter', '@variable')
end})

-- vgit
vim.o.updatetime = 300  -- recommended
vim.wo.signcolumn = 'yes'  -- show colored bars
mynvim.map_move('sh', 'Sh', function(backward)  -- "move to start of hunk"
  if vim.o.diff then
    m_shared.feedkeys(m_lua.if_(backward, '[c', ']c'))
  else
    local vgit = require('vgit')
    local line, column = m_shared.get_position()
    m_lua.if_(backward, vgit.hunk_up, vgit.hunk_down)()
    if (m_shared.get_position() - line) * m_lua.if_(backward, -1, 1) < 0 then
      m_shared.set_position(line, column)
    end
  end
end)
-- plugins

-- workarounds
vim.g.python_indent = {disable_parentheses_indenting=1}

if false then  -- experiments for consistent feedkeys
  --[[
  learnings
  - there is no way to escape insert mode immediately
    - workaround: enqueue <esc> and schedule function call
  - feedkeys with mode == 'nx' behaves like 'normal!' in normal and visual mode
    - processes input immediately
    - <esc> leaves visual mode


  feedkeys
  - mode == 'n'
    - enqueues keys (doesn't process immediately, l in insert mode inserts l)
  - mode == 'nx' and 'nx!'
    - processes keys (l in insert mode moves cursor to the right)
    - any key in insert mode produces quantum super position
  execute_normal
  - processes keys
  - <esc> doesn't leave insert mode
  --]]

  local f1
  f1 = function(string)
    return function()
      local line, column = m_shared.get_position()
      vim.fn.feedkeys(m_shared.replace_termcodes(string), 'nx')
      local new_line, new_column = m_shared.get_position()
      print(new_line ~= line or new_column ~= column)
    end
  end

  local f2
  f2 = function(string)
    return function()
      local mode = vim.api.nvim_get_mode()['mode']
      vim.fn.feedkeys(m_shared.replace_termcodes(string), 'nx')
      local new_mode = vim.api.nvim_get_mode()['mode']
      print(new_mode ~= mode)
    end
  end

  local function f3()
    vim.api.nvim_feedkeys(m_shared.replace_termcodes('<esc>'), 'n', false)
    f1('l')()
  end

  mynvim.map('<f1>', f1('l'), {'n', 'v', 'i'})
  mynvim.map('<f2>', f1('<cr>'), {'n', 'v', 'i'})
  mynvim.map('<f3>', f2('l'), {'n', 'v', 'i'})
  mynvim.map('<f4>', f2('<cr>'), {'n', 'v', 'i'})
  mynvim.map('<f5>', f2('<esc>'), {'n', 'v', 'i'})
  mynvim.map('<f6>', f3, {'n', 'v', 'i'})
  mynvim.map('<f12>', '<esc>', {'n', 'v', 'i'})
end
