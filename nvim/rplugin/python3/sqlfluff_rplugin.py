import difflib
import re

import pynvim


@pynvim.plugin
class Sqlfluff:
    _DIALECT = "oracle"

    def __init__(self, nvim):
        super().__init__()

        self.nvim = nvim

        self._sqlfluff_config = None

    @pynvim.command("SqlfluffV", sync=True, range=True)
    def _sqlfluff_v(self, range):
        import sqlfluff

        start, stop = range

        a = self.nvim.api.buf_get_lines(0, start - 1, stop, True)
        a_str = "\n".join(a)
        b = re.sub(
            "^",
            self._get_indent(a_str),
            sqlfluff.fix(a_str, config=self._get_sqlfluff_config()).strip(),
            flags=re.MULTILINE,
        ).split("\n")
        for tag, i1, i2, j1, j2 in reversed(
            difflib.SequenceMatcher(a=a, b=b, autojunk=False).get_opcodes()
        ):
            if tag == "equal":
                continue

            self.nvim.api.buf_set_lines(
                0, i1 + start - 1, i2 + start - 1, True, b[j1:j2]
            )

    @pynvim.command("SqlfluffVLint", sync=True, range=True)
    def _sqlfluff_v_lint(self, range):
        import sqlfluff

        start, stop = range
        violations = sqlfluff.lint(
            "\n".join(self.nvim.api.buf_get_lines(0, start - 1, stop, True)),
            config=self._get_sqlfluff_config(),
        )

        def _get_line(violation):
            return start + violation["start_line_no"] - 1

        line_width = max(len(str(_get_line(violation))) for violation in violations)
        column_width = max(
            len(str(violation["start_line_pos"])) for violation in violations
        )

        def _get_violation(violation):
            _start_line = _get_line(violation)
            _start_column = violation["start_line_pos"]
            _description = violation["description"]
            return f"{_start_line:{line_width}},{_start_column:{column_width}}: {_description}"

        _violations = "\n".join(_get_violation(violation) for violation in violations)
        self.nvim.out_write(f"{_violations}\n")

    @pynvim.command("Sqlfluff", sync=True)
    def _sqlfluff(self):
        import libcst
        import libcst.metadata as l_metadata
        import sqlfluff

        line_nr, column = self.nvim.api.win_get_cursor(0)
        lines = self.nvim.api.buf_get_lines(0, 0, -1, True)
        code_ranges = l_metadata.MetadataWrapper(
            libcst.parse_module("\n".join(lines))
        ).resolve(l_metadata.PositionProvider)
        for node, code_range in code_ranges.items():
            if (
                (
                    code_range.start.line < line_nr
                    or (
                        code_range.start.line == line_nr
                        and code_range.start.column <= column
                    )
                )
                and (
                    line_nr < code_range.end.line
                    or (
                        code_range.end.line == line_nr
                        and column < code_range.end.column
                    )
                )
                and isinstance(node, (libcst.SimpleString, libcst.FormattedString))
            ):
                break
        else:
            return

        match node:
            case libcst.SimpleString():
                current_code = node.value
                prefix = ""
                current_sql = node.raw_value

            case libcst.FormattedString():
                _lines = lines[code_range.start.line - 1 : code_range.end.line]
                if code_range.start.line == code_range.end.line:
                    _lines[0] = _lines[0][
                        code_range.start.column : code_range.end.column
                    ]

                else:
                    _lines[0] = _lines[0][code_range.start.column :]
                    _lines[-1] = _lines[-1][: code_range.end.column]

                current_code = "\n".join(_lines)
                prefix, current_sql = re.search(
                    "^(?P<prefix>\\w*)(?P<q>'+|\"+)(?P<sql>.*)(?P=q)$",
                    current_code,
                    flags=re.DOTALL,
                ).group("prefix", "sql")

        sql = sqlfluff.fix(current_sql, config=self._get_sqlfluff_config()).strip()

        indent = self._get_indent(current_sql)
        sql = re.sub(r"^", indent, sql, flags=re.MULTILINE)
        code = f'{prefix}"""\n{sql}\n{indent}"""' if "\n" in sql else f'{prefix}"{sql}"'
        if code != current_code:
            self.nvim.api.buf_set_text(
                0,
                code_range.start.line - 1,
                code_range.start.column,
                code_range.end.line - 1,
                code_range.end.column,
                code.split("\n"),
            )

    def _get_indent(self, sql):
        return " " * min(
            (
                len(match.group()) - 2
                for match in re.finditer(r"\n[ ]*\S", sql, flags=re.MULTILINE)
            ),
            default=0,
        )

    def _get_sqlfluff_config(self):
        if self._sqlfluff_config is not None:
            return self._sqlfluff_config

        import sqlfluff.core as s_core

        self._sqlfluff_config = s_core.FluffConfig.from_string(
            rf"""
[sqlfluff]
dialect = {Sqlfluff._DIALECT}
templater = placeholder
fix_even_unparsable = true

[sqlfluff:templater:placeholder]
param_regex=:\w+|\{{[^}}]*\}}
""".strip()
        )
        return self._sqlfluff_config
