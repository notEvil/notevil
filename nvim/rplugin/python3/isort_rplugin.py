import difflib
import pathlib

import pynvim


@pynvim.plugin
class Isort:
    def __init__(self, nvim):
        super().__init__()

        self.nvim = nvim

    @pynvim.command("Isort", sync=True)
    def _isort(self):
        import isort

        path = pathlib.Path(self.nvim.api.buf_get_name(0))

        a = self.nvim.api.buf_get_lines(0, 0, -1, True)
        b = isort.code(
            "\n".join(a),
            file_path=path,
            config=isort.Config(
                profile="black",
                combine_as_imports=True,
                src_paths=(
                    [_path]
                    if (_path := self._get_project_path(path)) is not None
                    else []
                ),
            ),
        ).split("\n")
        for tag, i1, i2, j1, j2 in reversed(
            difflib.SequenceMatcher(a=a, b=b, autojunk=False).get_opcodes()
        ):
            if tag == 'equal':
                continue

            self.nvim.api.buf_set_lines(0, i1, i2, True, b[j1:j2])

    def _get_project_path(self, path):
        while True:
            if (path / 'pyproject.toml').exists():
                return path

            if path.parent == path:
                return None

            path = path.parent
