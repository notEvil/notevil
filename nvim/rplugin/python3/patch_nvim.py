import dataclasses
import datetime
import pathlib
import re

import pynvim
from subprocess_shell import *


@dataclasses.dataclass
class _Patch:
    patch: pathlib.Path
    path: pathlib.Path

    _my_pulgins_path: str = dataclasses.field(repr=False)

    def __post_init__(self):
        if not isinstance(self.patch, pathlib.Path):
            self.patch = pathlib.Path(
                self.patch.replace("{my_plugins}", self._my_pulgins_path)
            )

        self.path = _get_path(self.path)


@dataclasses.dataclass
class _BeforeUpdateArgs:
    patches: list[_Patch]
    my_plugins_path: pathlib.Path

    def __post_init__(self):
        for index, patch in enumerate(self.patches):
            if not isinstance(patch, _Patch):
                self.patches[index] = _Patch(
                    _my_pulgins_path=str(self.my_plugins_path), **patch
                )

        self.my_plugins_path = _get_path(self.my_plugins_path)


@dataclasses.dataclass
class _AfterUpdateArgs:
    patches: list[_Patch]
    my_plugins_path: pathlib.Path
    reject: bool = False

    def __post_init__(self):
        for index, patch in enumerate(self.patches):
            if not isinstance(patch, _Patch):
                self.patches[index] = _Patch(
                    _my_pulgins_path=str(self.my_plugins_path), **patch
                )

        self.my_plugins_path = _get_path(self.my_plugins_path)


def _get_path(path: str | pathlib.Path) -> pathlib.Path:
    return path if isinstance(path, pathlib.Path) else pathlib.Path(path)


@pynvim.plugin
class Patch:
    def __init__(self, nvim):
        super().__init__()

        self.nvim = nvim

    @pynvim.function("PatchBeforeUpdate", sync=True)
    def _before_update(self, args):
        (_arg,) = args
        args = _BeforeUpdateArgs(**_arg)

        for patch in args.patches:
            patch_path = patch.patch
            if patch_path.exists():
                with open(patch_path, "r") as file:
                    source_patch = file.read()
            else:
                source_patch = None

            _patch = ["git", "diff", "HEAD", "--"] >> run(start(cwd=patch.path), read())
            target_patch = (
                re.sub(r"^index .*\n", "", _patch, flags=re.MULTILINE)
                if _patch != ""
                else None
            )

            if source_patch is not None:
                if target_patch is not None:
                    if source_patch != target_patch:
                        self.nvim.out_write(
                            f"updating patch, path={repr(str(patch_path))}\n"
                        )
                        _suffix = (
                            f".{datetime.datetime.now().isoformat()}{patch_path.suffix}"
                        )
                        patch_path.rename(patch_path.with_suffix(_suffix))

                        with open(patch_path, "w") as file:
                            file.write(target_patch)
            else:
                if target_patch is not None:
                    self.nvim.out_write(
                        f"updating patch, path={repr(str(patch_path))}\n"
                    )
                    with open(patch_path, "w") as file:
                        file.write(target_patch)

                else:
                    self.nvim.out_write(
                        f"WARN: patch is empty, path={repr(str(patch_path))}\n"
                    )

            _ = ["git", "restore", "--", "."] >> run(start(cwd=patch.path))

    @pynvim.function("PatchAfterUpdate", sync=True)
    def _after_update(self, args):
        (_arg,) = args
        args = _AfterUpdateArgs(**_arg)

        for patch in args.patches:
            if not patch.patch.exists():
                continue

            _args = ["git", "apply", "--reject" if args.reject else None, patch.patch]
            _ = list(filter(None, _args)) >> run(start(cwd=patch.path))
