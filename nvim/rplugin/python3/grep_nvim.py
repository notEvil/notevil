import pynvim
from subprocess_shell import *
import re
import shlex
import subprocess
import typing


MAX_ITEMS = 9999


@pynvim.plugin
class Grep:
    def __init__(self, nvim):
        super().__init__()

        self.nvim = nvim

    @pynvim.command("Grep", nargs=1, bang=True, complete="file_in_path")
    def _grep(self, args, bang):
        (argument,) = args
        argument = argument.strip()

        arguments = ["rg", "--smart-case", "--sort", "path", "--vimgrep"]
        if bang:
            arguments.extend(shlex.split(argument))
            patterns = []
            index = 0
            while index < len(arguments):
                if arguments[index] == "-e":
                    patterns.append(arguments[index + 1])
                    index += 2

                else:
                    index += 1

        else:
            arguments.extend(["--", argument])
            patterns = [argument]

        process = arguments >> start(stdin=subprocess.DEVNULL)
        if process >> wait(stdout=False, stderr=False, return_codes=None) not in (0, 1):
            self.nvim.err_write(b"".join(process.get_stderr_bytes()).decode())
            self.nvim.err_write("\n")
            return

        _v_ = zip(process.get_stdout_lines(bytes=True), range(MAX_ITEMS))
        lines = [line for line, _ in _v_]

        try:
            process.get_subprocess().wait(timeout=1)

        except subprocess.TimeoutExpired:
            process.get_subprocess().kill()
            self.nvim.err_write("Too many results\n")

        items = []
        for line in lines:
            _v_ = typing.cast(re.Match, re.search(rb"(.*?):(\d+):(\d+):(.*)", line))
            relative_path, line, column, text = _v_.groups()

            _v_ = dict(
                filename=relative_path.decode(),
                lnum=int(line.decode()),
                col=int(column.decode()),
                text=text,
            )
            items.append(_v_)

        items.sort(key=lambda d: tuple(d.values()))

        _v_ = dict(items=items, quickfixtextfunc="QuickfixText")
        self.nvim.funcs.setqflist([], "r", _v_)

        if len(items) == 0:
            self.nvim.api.cmd(dict(cmd="cclose"), {})

        else:
            self.nvim.api.cmd(dict(cmd="copen", mods=dict(split="botright")), {})
            self.nvim.funcs.clearmatches()
            pattern = "|".join(patterns)
            _case = r"\c" if pattern.islower() else r"\C"

            _v_ = pattern.replace(r"\b", "(<|>)").replace("??", "{-0,1}")
            _pattern = _v_.replace("*?", "{-0,}").replace("+?", "{-1,}")

            self.nvim.funcs.matchadd("GruvboxOrange", rf"\v{_case}{_pattern}")

        _count = f">= {MAX_ITEMS}" if len(items) == MAX_ITEMS else len(items)
        self.nvim.out_write(f"Grep found {_count} matches\n")

    @pynvim.function("QuickfixText", sync=True)
    def _quickfix_text(self, args):
        (arguments,) = args
        lines = []

        paths = {}
        previous_path = None
        quickfix_dicts = []

        _v_ = self.nvim.funcs.getqflist(dict(id=arguments["id"], items=True))["items"]
        for quickfix_dict in _v_[arguments["start_idx"] - 1 : arguments["end_idx"]]:
            buffer = quickfix_dict["bufnr"]
            path = paths.get(buffer)
            if path is None:
                path = self.nvim.funcs.bufname(buffer)
                paths[buffer] = path

            if path == previous_path:
                quickfix_dicts.append(quickfix_dict)

            else:
                lines.extend(self._format_dicts(quickfix_dicts, previous_path))

                quickfix_dicts.clear()
                quickfix_dicts.append(quickfix_dict)
                previous_path = path

        lines.extend(self._format_dicts(quickfix_dicts, previous_path))
        return lines

    def _format_dicts(self, quickfix_dicts, path):
        if len(quickfix_dicts) == 0:
            return

        width = len(str(quickfix_dicts[-1]["lnum"])) + 1
        for quickfix_dict in quickfix_dicts:
            line = quickfix_dict["lnum"]
            text = quickfix_dict["text"]
            yield f"{path}|{line:{width}}| {text}"
