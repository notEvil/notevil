local plugins = {}
for _, name in ipairs({
  'profile_nvim',
  'aerial_nvim',
  'black',
  'blame_nvim',
  'bufjump_nvim',
  'coc_nvim',
  'code_collapse',
  'comment_nvim',
  'dressing_nvim',
  'gitsigns_nvim',
  'grep',
  'gruvbox_nvim',
  'lazy_nvim',
  'legendary_nvim',
  'my',
  'my_plugins',
  'mynvim',
  'neo_tree_nvim',
  'nvim_dap',
  'nvim_dap_python',
  'nvim_dap_ui',
  'nvim_treesitter',
  'nvim_web_devicons',
  'oil_nvim',
  'patch',
  'python_execute',
  'smart_open_nvim',
  'telescope_coc_nvim',
  'telescope_nvim',
  'vgit_nvim',
  'vim_easymotion',
  'vim_matchup',
  'vim_surround',
}) do
  plugins[name] = require('my_plugins.' .. name)
end

plugins.lazy_nvim.setup_plugins(plugins, {})


local mynvim = require('mynvim')
local m_repeat_function = require('mynvim.repeat_function')
local m_shared = require('mynvim.shared')

vim.g.mapleader = ';'
vim.opt.conceallevel = 2                                             -- enable conceil
vim.opt.diffopt:append('linematch:60')                               -- align lines in diffs
vim.opt.expandtab = true                                             -- use spaces instead of tabs
vim.opt.ignorecase = true                                            -- in search
vim.opt.number = true                                                -- show line numbers
vim.opt.shiftwidth = 2                                               -- step size of horizontal shift
vim.opt.showcmd = false                                              -- don't show keys in status line
vim.opt.sidescrolloff = 20                                           -- scroll horizontally at end of line
vim.opt.smartcase = true                                             -- don't ignore case in search if pattern contains upper case
vim.opt.wildcharm = string.byte(m_shared.replace_termcodes('<tab>')) -- required for Tab to work in command mode
vim.opt.wrap = false                                                 -- don't wrap lines
vim.opt.wrapscan = false                                             -- don't wrap search at end of document


-- one down
mynvim.map('<a-n>', '<tab>', 'c')
mynvim.map('<a-n>', function()
  if vim.fn['coc#pum#visible']() == 0 then
    m_shared.feedkeys(vim.fn['coc#refresh']())      -- start autocomplete
  else
    m_shared.feedkeys(vim.fn['coc#pum#next'](true)) -- next suggestion
  end
end, 'i')
-- one up
mynvim.map('<a-e>', '<s-tab>', 'c')
mynvim.map('<a-e>', function()
  if vim.fn['coc#pum#visible']() == 0 then
    m_shared.feedkeys(vim.fn['coc#refresh']())      -- start autocomplete
  else
    m_shared.feedkeys(vim.fn['coc#pum#prev'](true)) -- previous suggestion
  end
end, 'i')
--
mynvim.map('<a-bs>', function()
  if vim.fn['coc#pum#visible']() == 0 then
    m_shared.feedkeys('<a-bs>')                   -- pass on
  else
    m_shared.feedkeys(vim.fn['coc#pum#cancel']()) -- cancel autocomplete
  end
end, 'i')
mynvim.map('<esc>', '<c-\\><c-n>', 't')                                     -- exit terminal mode
-- windows
mynvim.map('<a-E>', '<c-w>k', 'n')                                          -- "Edit window above"
mynvim.map('<a-H>', '<c-w>h', 'n')                                          -- "Edit window to the left"
mynvim.map('<a-I>', '<c-w>l', 'n')                                          -- "Edit window to the right"
mynvim.map('<a-N>', '<c-w>j', 'n')                                          -- "Edit window below"
-- diff
mynvim.map('<leader>dP', m_repeat_function.dot('dp'), 'n')                  -- "Diff put"
mynvim.map('<leader>dp', m_repeat_function.dot('do'), 'n')                  -- "Diff obtain"
mynvim.map('<leader>du', m_repeat_function.feeding(':dif<cr>'), 'n')        -- "Re-scan files for diffs"
-- misc
mynvim.map('<a-o>', '<esc>', { 'c', 'i', 'n', 't', 'v' }, { remap = true }) -- exit mode
mynvim.map('<leader>hd', ':nohlsearch<cr>', 'n')                            -- "Clear 'hlsearch' highlights"
mynvim.map('<leader>w=', '<c-w>=', 'n')                                     -- "All windows same size"
mynvim.map('<f5>', ':e<cr>', 'n')                                           -- "Reload file from disk"

vim.api.nvim_create_autocmd('VimEnter', {
  callback = function() -- set first buffer readonly, switch to second buffer and move to first hunk
    local count = 0
    for _, window in ipairs(vim.api.nvim_list_wins()) do
      if vim.api.nvim_win_get_option(window, 'diff') then
        count = count + 1
        if count == 2 then
          vim.api.nvim_set_current_win(window)
          m_shared.feedkeys('sh', true)
        else
          vim.api.nvim_buf_set_option(vim.api.nvim_win_get_buf(window), 'readonly', true)
        end
      end
    end
  end
})

-- workarounds
vim.g.python_indent = { disable_parentheses_indenting = 1 }
