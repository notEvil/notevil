return {
  setup = function(plugins)
    local mynvim = require('mynvim')

    mynvim.map('<f9>', function() require('dap').step_into() end, 'n')  -- "Step into"
    mynvim.map('<f10>', function() require('dap').step_over() end, 'n') -- "Step over"
    mynvim.map('<f11>', function() require('dap').step_out() end, 'n')  -- "Step out"
    mynvim.map('<f12>', function() require('dap').continue() end, 'n')  -- "Continue execution"
  end,
  legendary = {
    funcs = function(plugins)
      local m_shared = require('mynvim.shared')
      local mode = plugins.legendary_nvim.filters.mode

      return {
        {
          description = 'Step into',
          [1] = function() m_shared.feedkeys('<f9>') end,
          opts = { _keys = '<f9>' },
          filters = { mode.n }
        },
        {
          description = 'Step over',
          [1] = function() m_shared.feedkeys('<f10>') end,
          opts = { _keys = '<f10>' },
          filters = { mode.n }
        },
        {
          description = 'Step out',
          [1] = function() m_shared.feedkeys('<f11>') end,
          opts = { _keys = '<f11>' },
          filters = { mode.n }
        },
        {
          description = 'Continue execution',
          [1] = function() m_shared.feedkeys('<f12>') end,
          opts = { _keys = '<f12>' },
          filters = { mode.n }
        },
        {
          description = 'Toggle breakpoint',
          [1] = function() require('dap').toggle_breakpoint() end,
          filters = { mode.n }
        },
        {
          description = 'Fix breakpoints',
          [1] = function()
            require('dap').list_breakpoints()
            require('mynvim.shared').feedkeys(':botright cwindow<cr>')
          end,
          filters = { mode.n }
        },
        {
          description = 'Run to cursor',
          [1] = function() require('dap').run_to_cursor() end,
          filters = { mode.n }
        },
      }
    end,
  },
}
