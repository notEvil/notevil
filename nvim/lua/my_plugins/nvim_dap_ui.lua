return {
  lazy = {
    spec = {
      'rcarriga/nvim-dap-ui',
      dependencies = { 'mfussenegger/nvim-dap', 'nvim-neotest/nvim-nio' },
      opts = {
        mappings = {
          edit = 'i',
          expand = '<tab>',
          open = '<cr>',
          remove = 'd',
          repl = {},
          toggle = 'x',
        }
      }
    }
  },
  setup = function(plugins)
    local mynvim = require('mynvim')
    local m_repeat_function = require('mynvim.repeat_function')

    mynvim.map('<leader>dt', m_repeat_function.calling( -- "Toggle debugger"
      function() require('dapui').toggle() end
    ), 'n')
  end,
  legendary = {
    funcs = function(plugins)
      local m_shared = require('mynvim.shared')
      local mode = plugins.legendary_nvim.filters.mode

      return {
        {
          description = 'Toggle debugger',
          [1] = function() m_shared.feedkeys('<leader>dt', true) end,
          opts = { _keys = '<leader>dt' },
          filters = { mode.n },
        },
      }
    end,
  },
}
