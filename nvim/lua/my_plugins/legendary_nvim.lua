local function get_mode_filter(modes) -- show if in mode
  if type(modes) ~= 'table' then
    modes = { modes }
  end
  return function(item, context)
    local m_lua = require('mynvim.lua')

    return m_lua.get_key(modes, context.mode) or
        (m_lua.get_key(modes, 'v')
          and require('legendary.toolbox').is_visual_mode(context.mode))
  end
end

local _in = get_mode_filter({ 'i', 'n' })
local _n = get_mode_filter('n')
local _nv = get_mode_filter({ 'n', 'v' })
local _v = get_mode_filter('v')

local function get_not_buftype_filter(buftypes) -- show if in buftype
  if type(buftypes) ~= 'table' then
    buftypes = { buftypes }
  end
  return function(item, context)
    local m_lua = require('mynvim.lua')
    return m_lua.get_key(buftypes, context.buftype) == nil
  end
end

local _not_nofile = get_not_buftype_filter('nofile')

local function keymap_filter(item, context) -- show if keymap exists
  local m_shared = require('mynvim.shared')

  for _, keymap in ipairs(vim.api.nvim_buf_get_keymap(context.buf, context.mode)) do
    if keymap.lhs == m_shared.replace_termcodes(item.opts._keys) then
      return true
    end
  end
  return false
end

return {
  filters = {
    mode = {
      get_mode_filter = get_mode_filter,
      in_ = _in,
      n = _n,
      nv = _nv,
      v = _v,
    },
    buftype = {
      get_not_buftype_filter = get_not_buftype_filter,
      not_nofile = _not_nofile,
    },
    keymap = keymap_filter,
  },
  lazy = {
    spec = function(plugins)
      return {
        'mrjones2014/legendary.nvim',
        version = '*',
        dependencies = { 'kkharji/sqlite.lua' },
        opts = {
          funcs = function()
            local funcs = {}
            for _, plugin in pairs(plugins) do
              local legendary = plugin.legendary
              if legendary ~= nil then
                local _funcs = legendary.funcs
                if _funcs ~= nil then
                  for _, func in ipairs(_funcs(plugins)) do
                    table.insert(funcs, func)
                  end
                end
              end
            end
            return funcs
          end,
        }
      }
    end
  },
  patches = {
    {
      patch = '{my_plugins}/legendary_nvim.patch',
      path = vim.fn.stdpath('data') .. '/lazy/legendary.nvim',
    },
  },
  setup = function(plugins)
    local mynvim = require('mynvim')
    local m_repeat_function = require('mynvim.repeat_function')
    local m_shared = require('mynvim.shared')
    local _repeat = function() require('legendary').repeat_previous() end

    -- open menu
    mynvim.map('k', m_repeat_function.calling(
      function() require('legendary').find() end, { n = _repeat }
    ), 'n')
    mynvim.map('k', m_repeat_function.calling(
      function() require('legendary').find() end,
      { v = _repeat, V = _repeat, [m_shared.CTRL_V] = _repeat }
    ), 'v')
    for _, mode in ipairs({ 'c', 'i', 't' }) do
      mynvim.map('<a-k>', m_repeat_function.calling(
        function() require('legendary').find() end,
        { [mode] = _repeat }
      ), mode)
    end
  end,
}
