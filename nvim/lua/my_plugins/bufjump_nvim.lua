return {
  lazy = {
    spec = {
      'kwkarlwang/bufjump.nvim',
      opts = {
        forward_key = false,
        backward_key = false,
      }
    }
  },
  setup = function(plugins)
    local mynvim = require('mynvim')

    mynvim.map_move('\'', '"', function(backward) -- "Goto to newer/older cursor position in another buffer"
      local bufjump = require('bufjump')

      if vim.api.nvim_get_mode().mode ~= 'n' then
        return
      end
      if backward then
        bufjump.backward()
      else
        bufjump.forward()
      end
    end, 'n')
  end,
  legendary = {
    funcs = function(plugins)
      local m_shared = require('mynvim.shared')
      local mode = plugins.legendary_nvim.filters.mode

      return {
        {
          description = 'Move to newer cursor position in another buffer',
          [1] = function() m_shared.feedkeys('\'', true) end,
          opts = { _keys = '\'' },
          filters = { mode.n },
        },
        {
          description = 'Move to older cursor position in another buffer',
          [1] = function() m_shared.feedkeys('"', true) end,
          opts = { _keys = '"' },
          filters = { mode.n },
        },
      }
    end,
  },
}
