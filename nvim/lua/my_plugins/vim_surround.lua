return {
  lazy = {
    spec = 'tpope/vim-surround',
  },
  setup = function(plugins)
    local mynvim = require('mynvim')
    local m_repeat_function = require('mynvim.repeat_function')

    vim.g.surround_no_mappings = 1 -- disable default mappings
    mynvim.map('<leader>sc',       -- "Change Surround"
      m_repeat_function.feeding('<Plug>Csurround', true),
      'n')
    mynvim.map('<leader>sd', -- "Delete Surround"
      m_repeat_function.feeding('<Plug>Dsurround', true),
      'n')
    mynvim.map('<leader>si', -- "Insert Surround"
      m_repeat_function.feeding('<Plug>VSurround', true),
      'v')
  end,
  legendary = {
    funcs = function(plugins)
      local m_shared = require('mynvim.shared')
      local mode = plugins.legendary_nvim.filters.mode

      return {
        {
          description = 'Change Surround',
          [1] = function() m_shared.feedkeys('<leader>sc', true) end,
          opts = { _keys = '<leader>sc' },
          filters = { mode.n },
        },
        {
          description = 'Delete Surround',
          [1] = function() m_shared.feedkeys('<leader>sd', true) end,
          opts = { _keys = '<leader>sd' },
          filters = { mode.n },
        },
        {
          description = 'Insert Surround',
          [1] = function() m_shared.feedkeys('<leader>si', true) end,
          opts = { _keys = '<leader>si' },
          filters = { mode.v },
        },
      }
    end,
  },
}
