return {
  lazy = {
    spec = {
      'nvim-treesitter/nvim-treesitter',
      build = ':TSUpdate',
      config = function()
        require('nvim-treesitter.configs').setup({
          ensure_installed = {
            'd',
            'javascript',
            'markdown',
            'markdown_inline',
            'norg',
            'python',
            'typescript',
            'vue',
          },
          highlight = {
            enable = true,
            additional_vim_regex_highlighting = false,
          },
          incremental_selection = {
            enable = true,
            keymaps = {
              init_selection = false,
              node_incremental = false,
              scope_incremental = false,
              node_decremental = false
            },
          },
          matchup = {
            enable = true,
          },
        })
      end,
    },
  },
  patches = {
    {
      patch = '{my_plugins}/nvim_treesitter.patch',
      path = vim.fn.stdpath('data') .. '/lazy/nvim-treesitter',
    }
  },
  setup = function()
    local mynvim = require('mynvim')
    local m_lua = require('mynvim.lua')
    local m_repeat_move = require('mynvim.repeat_move')
    local m_shared = require('mynvim.shared')

    mynvim.map_move(']', '}', function(backward) -- "Move to next/previous node"
      local ts_utils = require('nvim-treesitter.ts_utils')

      local ts_node = ts_utils.get_node_at_cursor()
      if ts_node == nil then
        return
      end
      while true do -- find outer-most node
        local parent_node = ts_node:parent()
        if not (
              parent_node ~= nil
              and m_lua.are_equal(
                { parent_node:start() },
                { ts_node:start() }
              )
            ) then
          break
        end
        ts_node = parent_node
      end
      local line, column = m_shared.get_position()
      local line_index, column_index
      while true do
        local candidate_node = m_lua.if_(
          backward,
          ts_utils.get_previous_node,
          ts_utils.get_next_node
        )(ts_node, false, false)
        if candidate_node == nil then
          return
        end
        line_index, column_index = candidate_node:start()
        if not (line_index + 1 == line and column_index + 1 == column) then
          break
        end
        ts_node = candidate_node
      end
      m_shared.set_position(line_index + 1, column_index + 1)
    end)

    mynvim.map('{', function() -- "Move to parent node"
      local ts_utils = require('nvim-treesitter.ts_utils')

      m_repeat_move.move(true, function(backward)
        if not backward then
          return
        end

        local ts_node = ts_utils.get_node_at_cursor()
        if not (ts_node ~= nil and ts_node:type() ~= 'module') then
          return
        end
        local line, column = m_shared.get_position()
        local line_index, column_index
        while true do
          ts_node = ts_node:parent()
          if not (ts_node ~= nil and ts_node:type() ~= 'module') then
            return
          end
          line_index, column_index = ts_node:start()
          if not (line_index + 1 == line and column_index + 1 == column) then
            break
          end
        end
        m_shared.set_position(line_index + 1, column_index + 1)
      end)
    end, { 'n', 'v' })

    mynvim.map_move('[', '{', function(backward) -- "Select parent/child node"
      if not m_shared.is_visual_mode() then
        return
      end
      if backward then
        m_shared.feedkeys('<esc>')
        require('nvim-treesitter.incremental_selection').node_decremental()
        if vim.api.nvim_get_mode()['mode'] == 'n' then
          m_shared.feedkeys('gv')
        end
      else
        require('nvim-treesitter.incremental_selection').node_incremental()
      end
    end, 'v')

    vim.api.nvim_create_autocmd('FileType', {
      pattern = 'python',
      callback = function() -- adjust colors
        local _ = function(...)
          m_shared.execute_command('highlight', { 'link', ... })
        end
        _('@constant', '@variable')
        _('@constant.builtin', 'Number')
        _('@constructor', 'GruvboxGreen')
        _('@function', 'GruvboxYellow')
        _('@function.builtin', 'GruvboxGreen')
        _('@function.call', 'GruvboxGreen')
        _('@function.method', 'GruvboxYellow')
        _('@function.method.call', 'GruvboxGreen')
        _('@keyword.import', 'GruvboxRed')
        _('@string', 'GruvboxAqua')
        _('@string.escape', 'Number')
        _('@string.regexp', '@string')
        _('@type', '@variable')
        _('@variable.member', '@variable')
        _('@variable.parameter', '@variable')
        _('CocInlayHintType', 'GruvboxGray')
        _('CocInlayHintParameter', 'GruvboxGray')
      end
    })
    vim.api.nvim_create_autocmd({ 'BufRead', 'BufNewFile' }, {
      pattern = { '*.typ', '*.typ.jinja2' },
      callback = function() vim.o.ft = 'typst' end,
    })
    vim.api.nvim_create_autocmd({ 'BufRead', 'BufNewFile' }, {
      pattern = '*.tex.jinja2',
      callback = function() vim.o.ft = 'tex' end,
    })
  end,
  legendary = {
    funcs = function(plugins)
      local m_shared = require('mynvim.shared')
      local mode = plugins.legendary_nvim.filters.mode

      return {
        {
          description = 'Move to next node',
          [1] = function() m_shared.feedkeys(']', true) end,
          opts = { _keys = ']' },
          filters = { mode.nv },
        },
        {
          description = 'Move to previous node',
          [1] = function() m_shared.feedkeys('}', true) end,
          opts = { _keys = '}' },
          filters = { mode.nv },
        },
        {
          description = 'Move to parent node',
          [1] = function() m_shared.feedkeys('{', true) end,
          opts = { _keys = '{' },
          filters = { mode.n },
        },
        {
          description = 'Select parent node',
          [1] = function() m_shared.feedkeys('[', true) end,
          opts = { _keys = '[' },
          filters = { mode.v },
        },
        {
          description = 'Select child node',
          [1] = function() m_shared.feedkeys('{', true) end,
          opts = { _keys = '{' },
          filters = { mode.v },
        },
      }
    end,
  },
}
