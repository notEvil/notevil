return {
  lazy = {
    spec = {
      'numToStr/Comment.nvim',
      opts = {},
    }
  },
  setup = function(plugins)
    local mynvim = require('mynvim')
    local m_repeat_function = require('mynvim.repeat_function')

    -- "Toggle comment"
    mynvim.map('<leader>ct',
      m_repeat_function.feeding('<Plug>(comment_toggle_linewise_current)'),
      'n')
    mynvim.map('<leader>ct',
      m_repeat_function.feeding('<Plug>(comment_toggle_linewise_visual)'),
      'v')
  end,
  legendary = {
    funcs = function(plugins)
      local m_shared = require('mynvim.shared')
      local mode = plugins.legendary_nvim.filters.mode

      return {
        {
          description = 'Toggle comment',
          [1] = function() m_shared.feedkey('<leader>ct', true) end,
          opts = { _keys = '<leader>ct' },
          filters = { mode.nv },
        },
      }
    end,
  },
}
