local function get_patches(plugins)
  local patches = {}
  for _, plugin in pairs(plugins) do
    if plugin.patches ~= nil then
      for _, patch in ipairs(plugin.patches) do
        table.insert(patches, patch)
      end
    end
  end
  return patches
end

return {
  before_update = function(plugins)
    vim.api.nvim_call_function('PatchBeforeUpdate', { {
      patches = get_patches(plugins),
      my_plugins_path = plugins.my_plugins.path
    } })
  end,
  after_update = function(plugins, reject)
    if reject == nil then
      reject = false
    end
    vim.api.nvim_call_function('PatchAfterUpdate', { {
      patches = get_patches(plugins),
      my_plugins_path = plugins.my_plugins.path,
      reject = reject
    } })
  end
}
