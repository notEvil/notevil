return {
  lazy = {
    spec = {
      'neoclide/coc.nvim',
      branch = 'master',
      build = {
        'npm ci',
        ':CocInstall coc-lua',
        ':CocInstall coc-prettier', -- code formatter for many languages
        ':CocInstall coc-pyright',  -- Python
        ':CocInstall coc-rust-analyzer',
      }
    }
  },
  setup = function(plugins)
    local mynvim = require('mynvim')
    local m_lua = require('mynvim.lua')
    local m_repeat_function = require('mynvim.repeat_function')
    local m_shared = require('mynvim.shared')
    local config = vim.fn['coc#config']

    config('coc.preferences.extensionUpdateCheck', 'weekly')
    config('languageserver', {
      typst = {
        command = 'typst-lsp',
        filetypes = { 'typst' },
      }
    })
    config('pyright.inlayHints.functionReturnTypes', false)
    config('pyright.inlayHints.parameterTypes', false)
    config('pyright.inlayHints.variableTypes', false)
    config('rust-analyzer.inlayHints.parameterHints.enable', false)
    config('rust-analyzer.inlayHints.typeHints.enable', false)
    config('suggest.noselect', 1) -- start at first suggestion
    config('typescript.format.enable', false)
    config('vetur.format.defaultFormatter.js', 'prettier')

    mynvim.map('<Down>', function() -- "Scroll down one line"
      vim.fn['coc#float#scroll'](1, 1)
    end, { 'n', 'i' })
    mynvim.map('<Up>', function() -- "Scroll up one line"
      vim.fn['coc#float#scroll'](0, 1)
    end, { 'n', 'i' })
    mynvim.map('<PageDown>', function() -- "Scroll down one page"
      vim.fn['coc#float#scroll'](1)
    end, { 'n', 'i' })
    mynvim.map('<PageUp>', function() -- "Scroll up one page"
      vim.fn['coc#float#scroll'](0)
    end, { 'n', 'i' })
    mynvim.map('<leader>ds', m_repeat_function.calling( -- "Show documentation"
      function() vim.fn.CocAction('doHover') end
    ), 'n')
    mynvim.map('gd', m_repeat_function.calling( -- "Goto definition"
      function() vim.fn.CocAction('jumpDefinition') end
    ), 'n')
    mynvim.map('gr', m_repeat_function.calling( -- "Goto reference"
      function() vim.fn.CocAction('jumpUsed') end
    ), 'n')
    mynvim.map('<leader>rf', m_repeat_function.calling(function() -- "Fix references"
      local locations = vim.fn.CocAction('references', true)
      local list = {}
      for _, location in ipairs(locations) do
        assert(location.uri:sub(1, 7) == 'file://')
        table.insert(list, {
          filename = location.uri:gsub(
            '%%(%x%x)',
            function(a) return string.char(tonumber(a, 16)) end
          ):sub(8),
          lnum = location.range.start.line + 1,
          col = location.range.start.character + 1,
        })
      end
      table.sort(list, function(a, b)
        for _, name in ipairs({ 'filename', 'lnum', 'col' }) do
          local a_object = a[name]
          local b_object = b[name]
          if a_object < b_object then
            return true
          end
          if b_object < a_object then
            return false
          end
        end
        return false
      end)
      vim.fn.setqflist(list)
      m_shared.execute_command('cwindow')
    end), 'n')
    mynvim.map_move('si', 'Si', function(backward) -- "Move to start of next/previous issue
      m_shared.feedkeys(m_lua.if_(
        backward, '<Plug>(coc-diagnostic-prev)', '<Plug>(coc-diagnostic-next)'
      ))
    end)
  end,
  legendary = {
    funcs = function(plugins)
      local m_shared = require('mynvim.shared')
      local mode = plugins.legendary_nvim.filters.mode

      return {
        {
          description = 'Show documentation',
          [1] = function() m_shared.feedkeys('<leader>ds', true) end,
          opts = { _keys = '<leader>ds' },
          filters = { mode.n },
        },
        {
          description = 'Goto definition',
          [1] = function() m_shared.feedkeys('gd', true) end,
          opts = { _keys = 'gd' },
          filters = { mode.n },
        },
        {
          description = 'Goto reference',
          [1] = function() m_shared.feedkeys('gr', true) end,
          opts = { _keys = 'gr' },
          filters = { mode.n },
        },
        {
          description = 'Fix references',
          [1] = function() m_shared.feedkeys('<leader>rf', true) end,
          opts = { _keys = '<leader>rf' },
          filters = { mode.n },
        },
        {
          description = 'Move to start of next issue',
          [1] = function() m_shared.feedkeys('si', true) end,
          opts = { _keys = 'si' },
          filters = { mode.nv },
        },
        {
          description = 'Move to start of previous issue',
          [1] = function() m_shared.feedkeys('Si', true) end,
          opts = { _keys = 'Si' },
          filters = { mode.nv },
        },
      }
    end,
  },
}
