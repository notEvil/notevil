return {
  setup = function(plugins)
    vim.cmd([[
cnoreabbrev <expr> grep (getcmdtype() ==# ':' && getcmdline() ==# 'grep') ? 'Grep' : 'grep'
]])
  end,
}
