return {
  lazy = {
    spec = {
      'notEvil/code_collapse',
      url = 'https://gitlab.com/notEvil/code_collapse',
    }
  },
  setup = function(plugins)
    local mynvim = require('mynvim')
    local m_repeat_function = require('mynvim.repeat_function')

    mynvim.map('<leader>cc', -- "Collapse file with git diff"
      m_repeat_function.feeding(':CollapseFile git=1<cr>'),
      'n')
    mynvim.map('<leader>cc', -- "Collapse code"
      m_repeat_function.feeding(':CollapseCode<cr>'),
      'v')
    mynvim.map('<leader>cC', -- "Expand file with git diff"
      m_repeat_function.feeding(':CollapseFile git=1 expand=1<cr>'),
      'n')
    mynvim.map('<leader>cC', -- "Expand code"
      m_repeat_function.feeding(':CollapseCode expand=1<cr>'),
      'v')
  end,
  legendary = {
    funcs = function(plugins)
      local m_shared = require('mynvim.shared')
      local mode = plugins.legendary_nvim.filters.mode

      return {
        {
          description = 'Collapse code',
          [1] = function() m_shared.feedkeys('<leader>cc', true) end,
          opts = { _keys = '<leader>cc' },
          filters = { mode.v },
        },
        {
          description = 'Collapse file',
          [1] = function() m_shared.feedkeys(':CollapseFile<cr>') end,
          filters = { mode.n },
        },
        {
          description = 'Collapse file with git diff',
          [1] = function() m_shared.feedkeys('<leader>cc', true) end,
          opts = { _keys = '<leader>cc' },
          filters = { mode.n },
        },
        {
          description = 'Expand code',
          [1] = function() m_shared.feedkeys('<leader>cC', true) end,
          opts = { _keys = '<leader>cC' },
          filters = { mode.v },
        },
        {
          description = 'Expand file',
          [1] = function() m_shared.feedkeys(':CollapseFile expand=1<cr>') end,
          filters = { mode.n },
        },
        {
          description = 'Expand file with git diff',
          [1] = function() m_shared.feedkeys('<leader>cC', true) end,
          opts = { _keys = '<leader>cC' },
          filters = { mode.n },
        },
      }
    end,
  },
}
