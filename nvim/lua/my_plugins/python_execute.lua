return {
  setup = function(plugins)
    local mynvim = require('mynvim')

    vim.g.pe_auto_scope = 1
    vim.g.pe_auto_start = 1
    -- "Execute Python"
    mynvim.map('<a-cr>', '<c-o>:ExecutePython i<cr>', 'i')
    mynvim.map('<a-cr>', ':ExecutePython n<cr>', 'n')
    mynvim.map('<a-cr>', ':<c-u>ExecutePython v<cr>', 'v')
  end,
  legendary = {
    funcs = function(plugins)
      local m_shared = require('mynvim.shared')
      local mode = plugins.legendary_nvim.filters.mode

      return {
        {
          description = 'Execute Python',
          [1] = function() m_shared.feedkeys('<a-cr>', true) end,
          opts = { _keys = '<a-cr>' },
          filters = { mode.get_mode_filter({ 'i', 'n', 'v' }) },
        },
      }
    end,
  },
}
