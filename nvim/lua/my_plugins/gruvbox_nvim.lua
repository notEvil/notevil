return {
  lazy = {
    spec = {
      'ellisonleao/gruvbox.nvim',
      priority = 1000,
    }
  },
  colorscheme = 'gruvbox',
}
