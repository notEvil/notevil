return {
  lazy = {
    spec = {
      'FabijanZulj/blame.nvim',
      opts = {
        date_format = '%Y-%m-%d',
        mappings = {
          stack_push = '<bs>',
          stack_pop = '<tab>',
          show_commit = '<cr>',
          commit_info = '<leader>cs',
          close = {},
        },
      },
    },
  },
  patches = {
    {
      patch = '{my_plugins}/blame_nvim.patch',
      path = vim.fn.stdpath('data') .. '/lazy/blame.nvim',
    },
  },
  setup = function(plugins)
    local mynvim = require('mynvim')
    local m_repeat_function = require('mynvim.repeat_function')

    mynvim.map('<leader>gbt', -- "git: Toggle blame (blame)"
      m_repeat_function.feeding(':BlameToggle<cr>'),
      'n')
  end,
  legendary = {
    funcs = function(plugins)
      local m_shared = require('mynvim.shared')
      local mode = plugins.legendary_nvim.filters.mode

      return {
        {
          description = 'git: Toggle blame (blame)',
          [1] = function() m_shared.feedkeys('<leader>gbt') end,
          opts = { _keys = '<leader>gbt' },
          filters = { mode.n },
        },
      }
    end,
  },
}
