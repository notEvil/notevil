return {
  lazy = {
    spec = {
      'nvim-telescope/telescope.nvim',
      dependencies = { 'nvim-lua/plenary.nvim' },
      opts = {
        defaults = {
          layout_config = { prompt_position = 'top' },
          sorting_strategy = 'ascending',
          default_mappings = {
            i = {
              ['<a-n>'] = 'move_selection_next',
              ['<a-e>'] = 'move_selection_previous',
              ['<cr>'] = 'select_default',
              ['<esc>'] = 'close',
            },
          },
        },
      },
    },
  },
}
