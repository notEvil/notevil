-- from https://lazy.folke.io/installation

-- Bootstrap lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  local lazyrepo = "https://github.com/folke/lazy.nvim.git"
  local out = vim.fn.system({ "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath })
  if vim.v.shell_error ~= 0 then
    vim.api.nvim_echo({
      { "Failed to clone lazy.nvim:\n", "ErrorMsg" },
      { out,                            "WarningMsg" },
      { "\nPress any key to exit..." },
    }, true, {})
    vim.fn.getchar()
    os.exit(1)
  end
end
vim.opt.rtp:prepend(lazypath)
--


return {
  setup_plugins = function(plugins, config)
    if plugins.patch ~= nil then
      vim.api.nvim_create_autocmd('User', {
        pattern = { 'LazyInstallPre', 'LazyUpdatePre' },
        callback = function() plugins.patch.before_update(plugins) end,
      })
      vim.api.nvim_create_autocmd('User', {
        pattern = { 'LazyInstall', 'LazyUpdate' },
        callback = function()
          plugins.patch.after_update(plugins, config.patch_reject)
        end,
      })
    end

    local spec = {}
    local colorscheme
    for _, plugin in pairs(plugins) do
      local lazy = plugin.lazy
      if lazy ~= nil then
        local _spec = lazy.spec
        if _spec ~= nil then
          if type(_spec) == 'function' then
            _spec = _spec(plugins)
          end
          table.insert(spec, _spec)
        end
      end
      if plugin.colorscheme ~= nil then
        assert(colorscheme == nil)
        colorscheme = plugin.colorscheme
      end
    end

    local install = {}
    if colorscheme ~= nil then
      install.colorscheme = { colorscheme }
    end

    require('lazy').setup({
      spec = spec, install = install, checker = { enabled = true, notify = false }
    })

    if colorscheme ~= nil then
      require('mynvim.shared').execute_command('colorscheme', { colorscheme })
    end

    for _, plugin in pairs(plugins) do
      if plugin.immediate ~= nil then
        plugin.immediate()
      end
    end

    for _, plugin in pairs(plugins) do
      if plugin.setup ~= nil then
        plugin.setup(plugins)
      end
    end
  end
}
