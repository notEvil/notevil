return {
  lazy = {
    spec = {
      'stevearc/aerial.nvim',
      opts = {
        autojump = true,
        highlight_closest = false,
        highlight_mode = 'last',
        keymaps = {},
      }
    }
  },
  patches = {
    {
      patch = '{my_plugins}/aerial_nvim.patch',
      path = vim.fn.stdpath('data') .. '/lazy/aerial.nvim',
    }
  },
  setup = function(plugins)
    local mynvim = require('mynvim')
    local m_repeat_function = require('mynvim.repeat_function')

    mynvim.map('<leader>ot', -- "Toggle outline"
      m_repeat_function.feeding(':AerialToggle! right<cr>'),
      'n')
  end,
  legendary = {
    funcs = function(plugins)
      local m_shared = require('mynvim.shared')
      local mode = plugins.legendary_nvim.filters.mode

      return {
        {
          description = 'Toggle outline',
          [1] = function() m_shared.feedkeys('<leader>ot', true) end,
          opts = { _keys = '<leader>ot' },
          filters = { mode.n },
        },
      }
    end,
  },
}
