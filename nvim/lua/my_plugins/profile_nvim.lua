-- from https://github.com/stevearc/profile.nvim
local should_profile = os.getenv("NVIM_PROFILE")
if should_profile then
  require("profile").instrument_autocmds()
  if should_profile:lower():match("^start") then
    require("profile").start("*")
  else
    require("profile").instrument("*")
  end
end

local function toggle_profile()
  local prof = require("profile")
  if prof.is_recording() then
    prof.stop()
    vim.ui.input({ prompt = "Save profile to:", completion = "file", default = "profile.json" }, function(filename)
      if filename then
        prof.export(filename)
        vim.notify(string.format("Wrote %s", filename))
      end
    end)
  else
    prof.start("*")
  end
end
--

return {
  lazy = {
    spec = 'stevearc/profile.nvim',
  },
  setup = function(plugins)
    local mynvim = require('mynvim')

    mynvim.map('<f1>', toggle_profile, 'n') -- "Toggle profile"
  end,
  legendary = {
    funcs = function(plugins)
      local m_shared = require('mynvim.shared')
      local mode = plugins.legendary_nvim.filters.mode

      return {
        {
          description = 'Toggle profile',
          [1] = function() m_shared.feedkeys('<f1>', true) end,
          opts = { _keys = '<f1>' },
          filters = { mode.n },
        },
      }
    end,
  },
}
