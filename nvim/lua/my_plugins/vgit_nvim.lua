local function _git_move_page(backward)
  require('mynvim.shared').feedkeys(
    require('mynvim.lua').if_(backward, '<leader>_gplp=', '<leader>_gplp-'), true
  )
end

return {
  lazy = {
    spec = {
      'tanvirtin/vgit.nvim',
      dependencies = { 'nvim-lua/plenary.nvim' },
      opts = {
        settings = {
          live_blame = { enabled = false },
          live_gutter = { enabled = false },
          scene = { diff_preference = 'split' },
          diff_preview = {
            keymaps = {
              reset = '<leader>_gdpr',
              buffer_stage = '<leader>_gdpS',
              buffer_unstage = '<leader>_gdpU',
              buffer_hunk_stage = '<leader>_gdps',
              buffer_hunk_unstage = '<leader>_gdpu',
              toggle_view = '<leader>_gdpt',
            }
          },
          project_diff_preview = {
            keymaps = {
              commit = '<leader>_gpdpC',
              buffer_stage = '<leader>_gpdps',
              buffer_unstage = '<leader>_gpdpu',
              buffer_hunk_stage = '<leader>_gpdpgs',
              buffer_hunk_unstage = '<leader>_gpdpgu',
              buffer_reset = '<leader>_gpdpr',
              stage_all = '<leader>_gpdpS',
              unstage_all = '<leader>_gpdpU',
              reset_all = '<leader>_gpdpR',
            }
          },
          project_stash_preview = {
            keymaps = {
              add = '<leader>_gpspA',
              apply = '<leader>_gpspa',
              pop = '<leader>_gpspp',
              drop = '<leader>_gpspd',
              clear = '<leader>_gpspC'
            },
          },
          project_logs_preview = {
            keymaps = {
              previous = '<leader>_gplp-',
              next = '<leader>_gplp=',
            },
          },
          project_commit_preview = {
            keymaps = {
              save = '<leader>_gpcpS',
            }
          }
        }
      }
    }
  },
  legendary = {
    funcs = function(plugins)
      local m_repeat_move = require('mynvim.repeat_move')
      local m_shared = require('mynvim.shared')
      local filters = plugins.legendary_nvim.filters
      local buftype = filters.buftype
      local mode = filters.mode

      return {
        {
          description = 'git: Diff (vgit)',
          [1] = function() require('vgit').buffer_diff_preview() end,
          filters = { mode.n, buftype.not_nofile },
        },
        {
          description = 'git: Preview diff (vgit)',
          [1] = function() require('vgit').buffer_hunk_preview() end,
          filters = { mode.n, buftype.not_nofile },
        },
        {
          description = 'git: Preview blame (vgit)',
          [1] = function() require('vgit').buffer_blame_preview() end,
          filters = { mode.n, buftype.not_nofile },
        },
        {
          description = 'git: Log (vgit)',
          [1] = function() require('vgit').buffer_history_preview() end,
          filters = { mode.n, buftype.not_nofile },
        },
        {
          description = 'git: Stage all hunks (vgit)',
          [1] = function() require('vgit').buffer_stage() end,
          filters = { mode.n, buftype.not_nofile },
        },
        {
          description = 'git: Unstage all hunks (vgit)',
          [1] = function() require('vgit').buffer_unstage() end,
          filters = { mode.n, buftype.not_nofile },
        },
        {
          description = 'git: Reset all hunks (vgit)',
          [1] = function() require('vgit').buffer_reset() end,
          filters = { mode.n, buftype.not_nofile },
        },
        {
          description = 'git: Accept both (vgit)',
          [1] = function() require('vgit').buffer_conflict_accept_both() end,
          filters = { mode.n, buftype.not_nofile },
        },
        {
          description = 'git: Accept current (vgit)',
          [1] = function() require('vgit').buffer_conflict_accept_current() end,
          filters = { mode.n, buftype.not_nofile },
        },
        {
          description = 'git: Accept incoming (vgit)',
          [1] = function() require('vgit').buffer_conflict_accept_incoming() end,
          filters = { mode.n, buftype.not_nofile },
        },
        {
          description = 'git: Stage (vgit)',
          [1] = function() require('vgit').project_diff_preview() end,
          filters = { mode.n, buftype.not_nofile },
        },
        {
          description = 'git: Project log (vgit)',
          [1] = function() require('vgit').project_logs_preview() end,
          filters = { mode.n, buftype.not_nofile },
        },
        {
          description = 'git: Commit (vgit)',
          [1] = function() require('vgit').project_commit_preview() end,
          filters = { mode.n, buftype.not_nofile },
        },
        {
          description = 'git: Stash (vgit)',
          [1] = function() require('vgit').project_stash_preview() end,
          filters = { mode.n, buftype.not_nofile },
        },
        {
          description = 'git: Toggle diff preference (vgit)',
          [1] = function() require('vgit').toggle_diff_preference() end,
          filters = { mode.n },
        },
        {
          description = 'git: Toggle signs (vgit)',
          [1] = function() require('vgit').toggle_live_gutter() end,
          filters = { mode.n },
        },
        {
          description = 'git: Toggle blame (vgit)',
          [1] = function() require('vgit').toggle_live_blame() end,
          filters = { mode.n },
        },
        {
          description = 'git: Toggle debug logging (vgit)',
          [1] = function() require('vgit').toggle_tracing() end,
          filters = { mode.n },
        },

        {
          description = 'git: Reset all hunks (vgit)',
          [1] = function() m_shared.feedkeys('<leader>_gdpr', true) end,
          filters = { mode.n, filters.keymap },
          opts = { _keys = '<leader>_gdpr' },
        },
        {
          description = 'git: Stage all hunks (vgit)',
          [1] = function() m_shared.feedkeys('<leader>_gdpS', true) end,
          filters = { mode.n, filters.keymap },
          opts = { _keys = '<leader>_gdpS' },
        },
        {
          description = 'git: Unstage all hunks (vgit)',
          [1] = function() m_shared.feedkeys('<leader>_gdpU', true) end,
          filters = { mode.n, filters.keymap },
          opts = { _keys = '<leader>_gdpU' },
        },
        {
          description = 'git: Stage hunk (vgit)',
          [1] = function() m_shared.feedkeys('<leader>_gdps', true) end,
          filters = { mode.n, filters.keymap },
          opts = { _keys = '<leader>_gdps' },
        },
        {
          description = 'git: Unstage hunk (vgit)',
          [1] = function() m_shared.feedkeys('<leader>_gdpu', true) end,
          filters = { mode.n, filters.keymap },
          opts = { _keys = '<leader>_gdpu' },
        },
        {
          description = 'git: Toggle view (vgit)',
          [1] = function() m_shared.feedkeys('<leader>_gdpt', true) end,
          filters = { mode.n, filters.keymap },
          opts = { _keys = '<leader>_gdpt' },
        },
        {
          description = 'git: Commit (vgit)',
          [1] = function() m_shared.feedkeys('<leader>_gpdpC', true) end,
          filters = { mode.n, filters.keymap },
          opts = { _keys = '<leader>_gpdpC' },
        },
        {
          description = 'git: Stage file (vgit)',
          [1] = function() m_shared.feedkeys('<leader>_gpdps', true) end,
          filters = { mode.n, filters.keymap },
          opts = { _keys = '<leader>_gpdps' },
        },
        {
          description = 'git: Unstage file (vgit)',
          [1] = function() m_shared.feedkeys('<leader>_gpdpu', true) end,
          filters = { mode.n, filters.keymap },
          opts = { _keys = '<leader>_gpdpu' },
        },
        {
          description = 'git: Stage hunk (vgit)',
          [1] = function() m_shared.feedkeys('<leader>_gpdpgs', true) end,
          filters = { mode.n, filters.keymap },
          opts = { _keys = '<leader>_gpdpgs' },
        },
        {
          description = 'git: Unstage hunk (vgit)',
          [1] = function() m_shared.feedkeys('<leader>_gpdpgu', true) end,
          filters = { mode.n, filters.keymap },
          opts = { _keys = '<leader>_gpdpgu' },
        },
        {
          description = 'git: Reset file (vgit)',
          [1] = function() m_shared.feedkeys('<leader>_gpdpr', true) end,
          filters = { mode.n, filters.keymap },
          opts = { _keys = '<leader>_gpdpr' },
        },
        {
          description = 'git: Stage all files (vgit)',
          [1] = function() m_shared.feedkeys('<leader>_gpdpS', true) end,
          filters = { mode.n, filters.keymap },
          opts = { _keys = '<leader>_gpdpS' },
        },
        {
          description = 'git: Unstage all files (vgit)',
          [1] = function() m_shared.feedkeys('<leader>_gpdpU', true) end,
          filters = { mode.n, filters.keymap },
          opts = { _keys = '<leader>_gpdpU' },
        },
        {
          description = 'git: Reset all files (vgit)',
          [1] = function() m_shared.feedkeys('<leader>_gpdpR', true) end,
          filters = { mode.n, filters.keymap },
          opts = { _keys = '<leader>_gpdpR' },
        },
        {
          description = 'git: Add stash (vgit)',
          [1] = function() m_shared.feedkeys('<leader>_gpspA', true) end,
          filters = { mode.n, filters.keymap },
          opts = { _keys = '<leader>_gpspA' },
        },
        {
          description = 'git: Apply stash (vgit)',
          [1] = function() m_shared.feedkeys('<leader>_gpspa', true) end,
          filters = { mode.n, filters.keymap },
          opts = { _keys = '<leader>_gpspa' },
        },
        {
          description = 'git: Pop stash (vgit)',
          [1] = function() m_shared.feedkeys('<leader>_gpspp', true) end,
          filters = { mode.n, filters.keymap },
          opts = { _keys = '<leader>_gpspp' },
        },
        {
          description = 'git: Drop stash (vgit)',
          [1] = function() m_shared.feedkeys('<leader>_gpspd', true) end,
          filters = { mode.n, filters.keymap },
          opts = { _keys = '<leader>_gpspd' },
        },
        {
          description = 'git: Next page (vgit)',
          [1] = function() m_repeat_move.move(false, _git_move_page) end,
          filters = { mode.n, filters.keymap },
          opts = { _keys = '<leader>_gplp-' },
        },
        {
          description = 'git: Previous page (vgit)',
          [1] = function() m_repeat_move.move(true, _git_move_page) end,
          filters = { mode.n, filters.keymap },
          opts = { _keys = '<leader>_gplp=' },
        },
        {
          description = 'git: Save commit (vgit)',
          [1] = function() m_shared.feedkeys('<leader>_gpcpS', true) end,
          filters = { mode.n, filters.keymap },
          opts = { _keys = '<leader>_gpcpS' },
        },
      }
    end,
  }
}
