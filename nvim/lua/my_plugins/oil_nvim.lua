return {
  lazy = {
    spec = {
      'stevearc/oil.nvim',
      opts = {
        use_default_keymaps = false,
        view_options = { show_hidden = true },
        keymaps = {
          ['<f5>'] = 'actions.refresh',
          ['<cr>'] = 'actions.select',
          ['<bs>'] = 'actions.parent',
        },
      },
    },
  },
}
