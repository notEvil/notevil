return {
  setup = function(plugins)
    vim.g.EasyMotion_keys = 'esiroalpufywqmcxznt'
    vim.g.EasyMotion_smartcase = 1 -- case sensitive if pattern contains upper case
  end,
}
