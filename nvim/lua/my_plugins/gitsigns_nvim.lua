return {
  lazy = {
    spec = {
      'lewis6991/gitsigns.nvim',
      opts = {
        current_line_blame_opts = {
          delay = 100,
        },
      },
    },
  },
  legendary = {
    funcs = function(plugins)
      local m_shared = require('mynvim.shared')
      local mode = plugins.legendary_nvim.filters.mode

      return {
        {
          description = 'git: Toggle line blame (gitsigns)',
          [1] = function()
            m_shared.feedkeys(':Gitsigns toggle_current_line_blame<cr>')
          end,
          filters = { mode.n },
        },
        {
          description = 'git: Toggle staged (gitsigns)',
          [1] = function() m_shared.feedkeys(':Gitsigns stage_hunk<cr>') end,
          filters = { mode.nv },
        },
        {
          description = 'git: Reset hunk (gitsigns)',
          [1] = function() m_shared.feedkeys(':Gitsigns reset_hunk<cr>') end,
          filters = { mode.n },
        },
      }
    end,
  }
}
