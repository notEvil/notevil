return {
  lazy = {
    spec = {
      'psf/black',
      branch = 'stable',
    }
  },
  setup = function(plugins)
    vim.g.black_preview = 1                   -- use recently added features
    vim.g.black_skip_magic_trailing_comma = 1 -- disable forced line breaks if collection ends with comma
  end,
}
