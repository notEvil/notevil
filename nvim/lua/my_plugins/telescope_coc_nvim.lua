return {
  lazy = {
    spec = {
      'fannheyward/telescope-coc.nvim',
    },
  },
  setup = function(plugins)
    local mynvim = require('mynvim')
    local m_repeat_function = require('mynvim.repeat_function')

    mynvim.map('gs', m_repeat_function.calling(function() -- "Goto symbol"
      local telescope = require('telescope')
      local t_config = require('telescope.config')
      local generic_sorter = t_config.values.generic_sorter

      t_config.values.generic_sorter = telescope.extensions.fzy_native.native_fzy_sorter
      telescope.extensions.coc.workspace_symbols({})
      t_config.values.generic_sorter = generic_sorter
    end), 'n')
    mynvim.map('gi', m_repeat_function.calling( -- "Goto issue"
      function() require('telescope').extensions.coc.diagnostics() end
    ), 'n')
  end,
  legendary = {
    funcs = function(plugins)
      local m_shared = require('mynvim.shared')
      local mode = plugins.legendary_nvim.filters.mode

      return {
        {
          description = 'Goto symbol',
          [1] = function() m_shared.feedkeys('gs', true) end,
          opts = { _keys = 'gs' },
          filters = { mode.n },
        },
        {
          description = 'Goto issue',
          [1] = function() m_shared.feedkeys('gi', true) end,
          opts = { _keys = 'gi' },
          filters = { mode.n },
        },
      }
    end,
  },
}
