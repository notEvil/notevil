return {
  lazy = {
    spec = {
      'danielfalk/smart-open.nvim',
      branch = '0.2.x',
      dependencies = {
        'kkharji/sqlite.lua',
        'nvim-telescope/telescope.nvim',
        'nvim-telescope/telescope-fzy-native.nvim',
      },
    }
  },
  setup = function(plugins)
    local mynvim = require('mynvim')
    local m_repeat_function = require('mynvim.repeat_function')

    mynvim.map('gf', m_repeat_function.calling( -- "Goto file"
      function() require('telescope').extensions.smart_open.smart_open() end
    ), 'n')
  end,
  legendary = {
    funcs = function(plugins)
      local m_shared = require('mynvim.shared')
      local mode = plugins.legendary_nvim.filters.mode

      return {
        {
          description = 'Goto file',
          [1] = function() m_shared.feedkeys('gf', true) end,
          opts = { _keys = 'gd' },
          filters = { mode.n },
        },
      }
    end,
  },
}
