local function undo_change() -- "Undo change"
  -- in unified diff format
  local m_shared = require('mynvim.shared')

  m_shared.feedkeys('<esc>')
  local begin_line = m_shared.get_position('<')
  local end_line = m_shared.get_position('>')
  local lines = vim.api.nvim_buf_get_lines(0, begin_line - 1, end_line, true)
  local new_lines = {}
  for _, line in ipairs(lines) do
    if line:find('^+') ~= nil then
    elseif line:find('^-') then
      table.insert(new_lines, ' ' .. line:sub(2))
    else
      table.insert(new_lines, line)
    end
  end
  vim.api.nvim_buf_set_lines(0, begin_line - 1, end_line, true, new_lines)
end

local function get_hunk(number) -- "Get hunk from nth file"
  local m_shared = require('mynvim.shared')

  for _, window in ipairs(vim.api.nvim_list_wins()) do
    if vim.api.nvim_win_get_option(window, 'diff') then
      if number == 1 then
        m_shared.feedkeys(':diffget ' .. vim.api.nvim_win_get_buf(window) .. '<cr>')
        return
      else
        number = number - 1
      end
    end
  end
end

return {
  setup = function(plugins)
    local mynvim = require('mynvim')
    local m_lua = require('mynvim.lua')
    local m_repeat_function = require('mynvim.repeat_function')
    local m_shared = require('mynvim.shared')

    mynvim.map('<space>', function()
      -- autoformat file
      if vim.bo.filetype == 'python' then
        vim.api.nvim_cmd({ cmd = 'Black' }, {})              -- psf/black
        vim.api.nvim_cmd({ cmd = 'Isort' }, {})              -- rplugin
      else
        m_shared.feedkeys(':call CocAction(\'format\')<cr>') -- neoclide/coc.nvim
      end
    end, 'n')
    -- "Autoformat SQL"
    mynvim.map('<leader>s<space>', m_repeat_function.feeding(':Sqlfluff<cr>'), 'n')
    mynvim.map('<leader>s<space>', m_repeat_function.feeding(':SqlfluffV<cr>'), 'v')
    --
    mynvim.map('<f6>', ':e<cr>G', 'n')             -- "Reload file from disk and goto last line"
    mynvim.map_move('sh', 'Sh', function(backward) -- "Move to next/previous hunk"
      if vim.o.diff then
        m_shared.feedkeys(m_lua.if_(backward, '[c', ']c'))
      elseif vim.o.buftype == 'nofile' then
        local vgit = require('vgit')
        local line, column = m_shared.get_position()
        m_lua.if_(backward, vgit.hunk_up, vgit.hunk_down)()
        local new_line, _ = m_shared.get_position()
        if backward and line < new_line or not backward and new_line < line then
          m_shared.set_position(line, column)
        end
      else
        require('gitsigns').nav_hunk(
          m_lua.if_(backward, 'prev', 'next'), { wrap = false }
        )
      end
    end)
  end,
  legendary = {
    funcs = function(plugins)
      local m_shared = require('mynvim.shared')
      local mode = plugins.legendary_nvim.filters.mode

      return {
        {
          description = 'Undo change',
          [1] = undo_change,
          filters = { mode.v },
        },
        {
          description = 'Get hunk from 1st file',
          [1] = function() get_hunk(1) end,
          filters = { mode.n },
        },
        {
          description = 'Get hunk from 2nd file',
          [1] = function() get_hunk(2) end,
          filters = { mode.n },
        },
        {
          description = 'Get hunk from 3rd file',
          [1] = function() get_hunk(3) end,
          filters = { mode.n },
        },
        {
          description = 'Autoformat file',
          [1] = function() m_shared.feedkeys('<space>', true) end,
          opts = { _keys = '<space>' },
          filters = { mode.n },
        },
        {
          description = 'Autoformat SQL',
          [1] = function() m_shared.feedkeys('<leader>s<space>', true) end,
          opts = { _keys = '<leader>s<space>' },
          filters = { mode.nv },
        },
        {
          description = 'Reload file from disk and move to last line',
          [1] = function() m_shared.feedkeys('<f6>', true) end,
          opts = { _keys = '<f6>' },
          filters = { mode.n },
        },
        {
          description = 'Move to next hunk',
          [1] = function() m_shared.feedkeys('sh', true) end,
          opts = { _keys = 'sh' },
          filters = { mode.nv },
        },
        {
          description = 'Move to previous hunk',
          [1] = function() m_shared.feedkeys('Sh', true) end,
          opts = { _keys = 'Sh' },
          filters = { mode.nv },
        },
      }
    end,
  },
}
