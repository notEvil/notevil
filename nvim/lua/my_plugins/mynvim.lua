return {
  lazy = {
    spec = {
      'notEvil/mynvim',
      url = 'https://gitlab.com/notEvil/mynvim',
      dependencies = { 'notEvil/vim-easymotion' },
    }
  },
  immediate = function()
    require('mynvim').initialize(true)
  end,
}
