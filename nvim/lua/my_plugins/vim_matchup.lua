return {
  lazy = {
    spec = 'andymass/vim-matchup',
  },
  setup = function(plugins)
    local mynvim = require('mynvim')
    local m_repeat_move = require('mynvim.repeat_move')
    local m_shared = require('mynvim.shared')

    mynvim.map('(', function() -- "Move to next matchup"
      m_repeat_move.move(false, function(backward)
        if backward then
          return
        end
        m_shared.feedkeys('<plug>(matchup-%)', true)
      end)
    end, { 'n', 'v' })
  end,
  legendary = {
    funcs = function(plugins)
      local m_shared = require('mynvim.shared')
      local mode = plugins.legendary_nvim.filters.mode

      return {
        {
          description = 'Move to next matchup',
          [1] = function() m_shared.feedkeys('(', true) end,
          opts = { _keys = '(' },
          filters = { mode.n },
        },
      }
    end,
  },
}
