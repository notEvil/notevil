local function _neotree_move(backward, state)
  local m_shared = require('mynvim.shared')

  local line, column = m_shared.get_position()
  if backward then
    state.commands.prev_git_modified(state)
  else
    state.commands.next_git_modified(state)
  end
  local new_line, _ = m_shared.get_position()
  if backward ~= (new_line < line) then -- don't wrap
    m_shared.set_position(line, column)
  end
end

return {
  lazy = {
    spec = {
      'nvim-neo-tree/neo-tree.nvim',
      branch = 'v3.x',
      dependencies = { 'nvim-lua/plenary.nvim', 'MunifTanjim/nui.nvim' },
      opts = {
        use_default_mappings = false,
        filesystem = {
          hijack_netrw_behavior = 'disabled',
          filtered_items = { visible = true },
          follow_current_file = { enabled = true },
          use_libuv_file_watcher = true,
        },
        window = {
          mappings = {
            ['<f5>'] = 'refresh',
            ['<tab>'] = 'toggle_node',
            ['<bs>'] = 'close_node',
            ['<cr>'] = function(state)
              require('neo-tree.sources.common.commands').open(state, 'edit')
            end,
            ['sh'] = function(state)
              require('mynvim.repeat_move').move(false, function(backward)
                _neotree_move(backward, state)
              end)
            end,
            ['Sh'] = function(state)
              require('mynvim.repeat_move').move(true, function(backward)
                _neotree_move(backward, state)
              end)
            end,
          }
        },
      },
    },
  },
  patches = {
    {
      patch = '{my_plugins}/neo_tree_nvim.patch',
      path = vim.fn.stdpath('data') .. '/lazy/neo-tree.nvim',
    }
  },
  setup = function(plugins)
    local mynvim = require('mynvim')
    local m_repeat_function = require('mynvim.repeat_function')

    mynvim.map('<leader>tt', -- "Toggle tree"
      m_repeat_function.feeding(':Neotree toggle action=show<cr>'),
      'n')
  end,
  legendary = {
    funcs = function(plugins)
      local m_shared = require('mynvim.shared')
      local mode = plugins.legendary_nvim.filters.mode

      return {
        {
          description = 'Toggle tree',
          [1] = function() m_shared.feedkeys('<leader>tt', true) end,
          opts = { _keys = '<leader>tt' },
          filters = { mode.n },
        },
        {
          description = 'tree: Use filesystem',
          [1] = function() m_shared.feedkeys(':Neotree source=filesystem<cr>') end,
          filters = { mode.n },
        },
        {
          description = 'tree: Use buffers',
          [1] = function() m_shared.feedkeys(':Neotree source=buffers<cr>') end,
          filters = { mode.n },
        },
        {
          description = 'tree: Use Git status',
          [1] = function() m_shared.feedkeys(':Neotree source=git_status<cr>') end,
          filters = { mode.n },
        },
      }
    end,
  },
}
